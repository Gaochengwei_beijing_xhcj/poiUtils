package com.test;


import com.test.Bean.ExcelOutputBean;
import com.xiaominge.utils.excleUtils.excelMerge.ExcelCellInfo;
import com.xiaominge.utils.excleUtils.excelMerge.ExcelMergeData;
import com.xiaominge.utils.excleUtils.excelMerge.MergeExcelData;
import com.xiaominge.utils.excleUtils.excelOut.ExcelData;
import com.xiaominge.utils.excleUtils.excelOut.ExcelOutputUtils;
import com.xiaominge.utils.excleUtils.excelOut.web.ExcelOutWeb;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.xiaominge.utils.excleUtils.excelMerge.ExcelMergeOutputUtils.exportExcel;

/**
 * excel输出
 *
 * @author 11231
 * @date 2022/02/09
 */
public class ExcelOutput {

    private String xlsxSavePath = "D:/桌面/测试.xlsx";

    /**
     * web 界面选择导出测试
     * 前端传 字段名 以及标题 进行导出
     */
    @Test
    public void webTest() throws IOException, IllegalAccessException {
        ExcelOutWeb<ExcelOutputBean> excelOutputBeanExcelOutWeb = new ExcelOutWeb<>(ExcelOutputBean.class);
        List<ExcelOutputBean> excelOutputBeans = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            ExcelOutputBean excelOutputBean = new ExcelOutputBean();
            excelOutputBean.setName("名字" + i);
            excelOutputBean.setSex(String.valueOf(i / 2));
            excelOutputBean.setPhone(1 + 1000);
            excelOutputBean.setDate(new Date());
            excelOutputBeans.add(excelOutputBean);
        }
        //模拟界面选择 两个
        HashMap<String, String> fieldToDesc = new HashMap<>();
        fieldToDesc.put("name", "名称");
        fieldToDesc.put("sex", "性别");
        excelOutputBeanExcelOutWeb.exportExcel(new FileOutputStream(xlsxSavePath), excelOutputBeans, fieldToDesc, ExcelOutWeb.ExcelType.xlsx);
    }

    /**
     * 后台数据导出测试
     */
    @Test
    public void dataOutTest() throws IOException {
        //excel 数据
        ExcelData excelData = new ExcelData();
        //创建标题
        List<String> titles = excelData.CreateTitle();
        titles.add("名字");
        titles.add("性别");
        titles.add("电话");
        titles.add("时间");
        for (int i = 0; i < 10; i++) {
            //创建行
            List<ExcelData.CellDataInfo> oneRow = excelData.createOneRow();
            //按顺序 给行添加单元格
            oneRow.add(excelData.createCellDataInfo("名字" + i));
            oneRow.add(excelData.createCellDataInfo(i / 2));
            oneRow.add(excelData.createCellDataInfo(100000 + i));
            //  oneRow.add(excelData.createCellDataInfo(LocalDate.now()));
            // oneRow.add(excelData.createCellDataInfo(LocalDateTime.now()));
            oneRow.add(excelData.createCellDataInfo(new Date()));
        }
        ExcelOutputUtils.exportExcel(ExcelOutputUtils.getWorkbook(ExcelOutputUtils.ExcelType.xlsx), excelData, new FileOutputStream(xlsxSavePath));
    }

    /**
     * 合并单元测试
     */
    @Test
    public void mergeCellTest() throws IOException {
        MergeExcelData mergeExcelData = new MergeExcelData();

        //创建标题单元格
        List<ExcelCellInfo> titleCells = new ArrayList<>();
        titleCells.add(new ExcelCellInfo("名字", 0));
        titleCells.add(new ExcelCellInfo("性别", 1));
        titleCells.add(new ExcelCellInfo("电话", 2, new ExcelMergeData(0, 2, 4))); // 2 3 4 进行合并
        titleCells.add(new ExcelCellInfo("时间", 5)); //5



        //行号 第0行是标题
        int rowIndex = 0;
        //创建并设置行
        mergeExcelData.setTitle(mergeExcelData.createOneRow(rowIndex, titleCells));



        for (int i = 0; i < 10; i++) {
            //一行的单元格
            if (i==5){
                //第6行数据 进行一个上下合并
                List<ExcelCellInfo> oneRowDataCell = new ArrayList<>();
                oneRowDataCell.add(new ExcelCellInfo("名字"+i, 0, new ExcelMergeData(2, 0, 0)));
                oneRowDataCell.add(new ExcelCellInfo("性别"+i, 1, new ExcelMergeData(2, 1, 1)));
                oneRowDataCell.add(new ExcelCellInfo("电话"+i, 2, new ExcelMergeData(2, 2, 4))); // 2 3 4 列进行合并 并且向下合并两行
                oneRowDataCell.add(new ExcelCellInfo("时间"+i, 5,new ExcelMergeData(2, 5, 5))); //5
                rowIndex =rowIndex+1;
                mergeExcelData.addRowDate(mergeExcelData.createOneRow(rowIndex, oneRowDataCell));
                rowIndex+=2;//向下合并两个 下面的两行不能写入数据 写入数据会被合并单元格覆盖
            }else {
                List<ExcelCellInfo> oneRowDataCell = new ArrayList<>();
                oneRowDataCell.add(new ExcelCellInfo("名字"+i, 0));
                oneRowDataCell.add(new ExcelCellInfo("性别"+i, 1));
                oneRowDataCell.add(new ExcelCellInfo("电话"+i, 2, new ExcelMergeData(0, 2, 4))); // 2 3 4 列进行合并
                oneRowDataCell.add(new ExcelCellInfo("时间"+i, 5)); //5
                rowIndex +=1;
                mergeExcelData.addRowDate(mergeExcelData.createOneRow(rowIndex, oneRowDataCell));
            }
        }
        List<ExcelCellInfo> oneRowDataCell = new ArrayList<>();
        oneRowDataCell.add(new ExcelCellInfo("名字"+6, 0));
        oneRowDataCell.add(new ExcelCellInfo("性别"+6, 1));
        oneRowDataCell.add(new ExcelCellInfo("电话"+6, 2, new ExcelMergeData(0, 2, 4))); // 2 3 4 列进行合并
        oneRowDataCell.add(new ExcelCellInfo("时间"+6, 5)); //5
        mergeExcelData.addRowDate(mergeExcelData.createOneRow(rowIndex+1, oneRowDataCell));
        oneRowDataCell.add(new ExcelCellInfo(new FileInputStream("D:\\Backup\\Documents\\My Pictures\\Snipaste_2023-01-01_14-16-33.png"), Workbook.PICTURE_TYPE_PNG,6)); //5
        exportExcel("D://桌面","ces.xlsx", mergeExcelData);
       // exportExcel(ExcelMergeOutputUtils.getWorkbook(ExcelMergeOutputUtils.ExcelType.xlsx), mergeExcelData, new FileOutputStream(xlsxSavePath));

    }
}
