package com.test;


import com.test.Bean.ExcelInputBean;
import com.xiaominge.utils.excleUtils.excelInput.ExcelAnnotationUtils;
import com.xiaominge.utils.excleUtils.excelInput.ExcelInputUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * excel 读取
 *
 * @author 11231
 * @date 2022/01/20
 */

@Slf4j
public class ExcelInput {


    private InputStream inputStream = this.getClass().getResourceAsStream("/excelInput/excelInput.xlsx");


    /**
     * 测试读取excel 为hashMap
     * 字段名称的顺序 和excel 列的顺序一致
     */
    @Test
    public void testMap() throws IOException {
        List<Map<String, String>> maps = ExcelInputUtils.readToListMap(inputStream, "name", "sex", "phone", "data");
        System.out.println(maps);
    }


    /**
     * 测试得到输入模板
     * <p>
     * 根据对象的 注解的index 和title 获取填写的excel 模板
     */
    @Test
    public void testGetInputTemplate() throws FileNotFoundException {
        ExcelAnnotationUtils<ExcelInputBean> excelInputBeanExcelAnnotationUtils = new ExcelAnnotationUtils<>(ExcelInputBean::new);
        excelInputBeanExcelAnnotationUtils.downloadTemplateExcel(new FileOutputStream("D:/桌面/excel填写模板.xlsx"), ExcelAnnotationUtils.ExcelType.xlsx);
    }

    /**
     * 测试将excel 根据Bean 注解 读取为对象
     */
    @Test
    public void TestInputBean() throws Exception {
        ExcelAnnotationUtils<ExcelInputBean> inputBeanExcelAnnotationUtils = new ExcelAnnotationUtils<>(ExcelInputBean::new);
        List<ExcelInputBean> excelInputBeans = inputBeanExcelAnnotationUtils.importExcel(inputStream);
        System.out.println("结果:" + excelInputBeans);
    }
}


