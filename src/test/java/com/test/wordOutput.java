package com.test;

import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.utils.WordTemplateUtils;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.utils.operationUtils.WordValueEnum;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.WordInfo;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.tableBean.*;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.textBean.OneFor;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.textBean.Text;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.textBean.TextFroReaders;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;

/**
 * @program: poi-utils
 * @description: word 根据模板导出测试
 * @author: xiaominge
 * @create: 2022-01-17 15:34
 **/

public class wordOutput {
    @Test
    public void outTest() throws Exception {

        WordInfo wordInfo = new WordInfo();

        //直接渲染文字
        Text text = wordInfo.createText();
        text.putValue("name", "张三");
        text.putValue("sex", "男");

        //渲染图片
        text.putImage(
                "image_head",
                "png",
                this.getClass().getResourceAsStream("/wordOutput/img.png"),
                100,
                100,
                false
        );


        //循环渲染文字
        TextFroReaders textFroReaders = wordInfo.createTextFroReaders();

        //创建一个名称的循环替换 对应模板 ##{forEachKey_user}##
        OneFor user = textFroReaders.CreateOneForText("user");
        for (int i = 0; i < 2; i++) {
            Text oneForText = user.createOneForText();
            oneForText.putValue("userName", "无下划线的文字<word_underline> 有下换线中间的文字 </word_underline> 下划线之后的文字 <word_bold> 加粗文字</word_bold>" + i);
        }


        //创建一个名称的循环替换  对应 ##{forEachKey_card}##
        OneFor card = textFroReaders.CreateOneForText("card");
        for (int i = 0; i < 2; i++) {
            Text oneForText = card.createOneForText();
            oneForText.putValue("cardNumber", 12 + i);
        }
        text.putValue("idCard", 123456789, true);
        //创建表格对象
        TableBeans tableBean = wordInfo.createTableBean();

        //  创建 一个基本的表格 简单替换
        OneTable table_1 = tableBean.CreateOneTable("table_1", TableType.replace_value);
        //创建一行数据
        OneRow oneRow = table_1.createEndRow();
        oneRow.createCell("index", 1);
        oneRow.createCell("name", "表格1名字");
        oneRow.createCell("number", "表格1编号1234");

        //创建行循环
        OneTable table_2 = tableBean.CreateOneTable("table_2", TableType.for_Row);
        //创建一行数据
        for (int i = 0; i < 5; i++) {
            OneRow table_2_oneRow = table_2.createNewOneRow();
            table_2_oneRow.createCell("index", 1);
            table_2_oneRow.createCell("name", "表格2名字" + i);
            table_2_oneRow.createCell("number", "表格2编号1234" + i);
        }

        //创建表格循环
        OneTable table_3 = tableBean.CreateOneTable("table_3", TableType.for_table);
        //创建一行数据
        for (int i = 0; i < 3; i++) {
            OneRow table_3OneRow = table_3.createNewOneRow();
            table_3OneRow.createCell("index", 1);
            table_3OneRow.createCell("name", "表格3名字" + i);
            table_3OneRow.createCell("number", "表格3编号" + i);
        }


        //创建合并单元格  合并单元格 只有在行循环的表格中生效 因为其他模式下不涉代码合并单元格 在模板中就可以制作号表格
        OneTable table_4 = tableBean.CreateOneTable("table_4", TableType.for_Row);
        for (int i = 0; i < 20; i++) {
            OneRow table_4_oneRow = table_4.createNewOneRow();
            OneCell indexCell = table_4_oneRow.createCell("index", i);

            //第一行 的第一列 和第二列  进行合并 合并之后 只保存第一列的值
            if (i == 0) {
                indexCell.createCellMergeBean(1, 2);
            }
            if (i == 2) {
                //第三行 第四行 第五行  的第一列进行合并
                indexCell.createRowMergeBean(2, 1);
            }

            //i=7   第一列 第二列 向下合并四行  当前行的数据进行补充 所以总行数是5  进行合并
            if (i == 7) {
                indexCell.createCellMergeBean(5, 1, 2);

            }

            table_4_oneRow.createCell("name", "表格4合并单元格名字" + i);
            table_4_oneRow.createCell("number", "测试下换线"+WordValueEnum.UnderLineStart.getValue()+"表格4合并单元格编号" + WordValueEnum.UnderLineEnd.getValue() + i);
        }

        //加载模板
        WordTemplateUtils wordTemplateUtils = new WordTemplateUtils(this.getClass().getResourceAsStream("/wordOutput/template.docx"));
        //变量替换
         wordTemplateUtils.replaceDocument(wordInfo);
        FileOutputStream fileOutputStream = new FileOutputStream("D:\\桌面\\aa123.docx");
        //进行输出
        wordTemplateUtils.write(fileOutputStream);
    }
}
