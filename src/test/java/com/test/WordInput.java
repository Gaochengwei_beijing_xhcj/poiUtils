package com.test;

import com.xiaominge.utils.wordUtils.wordInputUtils.WordInfoBean;
import com.xiaominge.utils.wordUtils.wordInputUtils.WordToHtmlUtils;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.InputStream;

/**
 * @program: poi-utils
 * @description: word 读取测试
 * @author: xiaominge
 * @create: 2022-01-17 11:14
 **/

@Slf4j
public class WordInput {

    /**
     * 获取当前项目的地址
     */
    private static final String imagesavePath = System.getProperty("user.dir") + "/wordInputImageSaveDir";


    /**
     * 读取 docx 测试
     * @throws Exception
     */
    @Test
    public void readDocx() throws Exception {
        InputStream docx = WordInput.class.getResourceAsStream("/wordInput/1.docx");
        WordInfoBean docxToHtml = WordToHtmlUtils.docxToHtml(docx, "1.docx", imagesavePath+"/1-docx");
        log.info("docx 读取结果:{}", docxToHtml);
    }

    /**
     * 读取doc 测试
     * @throws Exception
     */
    @Test
    public void readDoc() throws Exception {
        InputStream doc = WordInput.class.getResourceAsStream("/wordInput/1.doc");
        WordInfoBean docToHtml = WordToHtmlUtils.docToHtml(doc, "1.doc", imagesavePath+"/1-doc");
        log.info("doc 读取结果:{}", docToHtml);
    }
}
