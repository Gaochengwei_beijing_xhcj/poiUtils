package com.test.Bean;

import lombok.Data;

import java.util.Date;

@Data
public class ExcelOutputBean {
    private String name;
    private String sex;
    private int phone;
    private Date date;
}
