package com.test.Bean;

import com.xiaominge.utils.excleUtils.excelInput.annotation.ExcelField;
import lombok.Data;

import java.util.Date;

@Data
public class ExcelInputBean {
    @ExcelField(index = 1, title = "姓名")
    private String name;
    @ExcelField(index = 2, title = "性别")
    private String sex;
    @ExcelField(index = 3, title = "电话")
    private int phone;
    @ExcelField(index = 4, title = "日期")
    private Date date;
}
