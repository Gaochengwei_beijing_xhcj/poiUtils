package com.test.Bean;

import com.xiaominge.utils.pdf.PdfMergeUtils;
import com.xiaominge.utils.pdf.PdfUtils;
import com.xiaominge.utils.wordUtils.WordToPdfUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: poiUtils
 * @description:
 * @author: lzm word 转pdf 测试
 * @create: 2022-07-05 10:02
 **/

@Slf4j
public class WordToPDF {

    /**
     * docx 转pdf
     */
    @SneakyThrows
    @Test
    public void docxToPdf() {
        WordToPdfUtils.getUtils().
                docxToPdf(this.getClass().getResourceAsStream("/wordInput/1.docx"),
                        new FileOutputStream("D://桌面/1.pdf"));
    }

    /**
     * pdf合并
     */
    @SneakyThrows
    @Test
    public void pdfMerge() {
        List<File> files = new ArrayList() {{
            add(new File("D://桌面/1.pdf"));
            add(new File("D://桌面/1.pdf"));
        }};
        PdfMergeUtils.getUtils().PDFMerge(files, new FileOutputStream("D://桌面/mergePdf.pdf"));
    }

    /**
     * 得到pdf页码
     */
    @Test
    public void getPDFSize() {
        int pdfTotalSize = PdfUtils.getPUtils().getPdfTotalSize(new File("D://桌面/mergePdf.pdf"));
        log.info("总页数: {}", pdfTotalSize);
    }

}
