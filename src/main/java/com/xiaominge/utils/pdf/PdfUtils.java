package com.xiaominge.utils.pdf;

import com.aspose.words.Document;
import com.aspose.words.License;
import com.xiaominge.exception.ParameterRuntimeException;
import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @program: system_platform
 * @description: PdfUtils
 * @author: xiaominge
 * @create: 2021-01-13 16:49
 **/

public class PdfUtils {

    private  volatile static PdfUtils pdfUtils;

    public static PdfUtils getPUtils() {
        if (pdfUtils == null) {
            synchronized (PdfUtils.class) {
                if (pdfUtils == null) {
                    pdfUtils = new PdfUtils();
                }
            }
        }
        return pdfUtils;
    }

    public int getPdfTotalSize(File file) {

        if (file == null) {
            ParameterRuntimeException.throwException("文件不能为空");
        }
        String fileName = file.getName();
        String filePostFix= fileName.substring(fileName.lastIndexOf(".") + 1);

        if (!"pdf".equalsIgnoreCase(filePostFix)) {

            ParameterRuntimeException.throwException("不是pdf文件,无法获取总页数");
        }
        PDDocument pdfReader = null;
        try {
            pdfReader = PDDocument.load(file);
        } catch (IOException e) {
            e.printStackTrace();
            ParameterRuntimeException.throwException("不是pdf文件,无法获取总页数");

        }
        int numberOfPages = pdfReader.getNumberOfPages();
        try {
            pdfReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                pdfReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return numberOfPages;
    }


    /**
     *    // 全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
     * @param inputStream
     * @param outputStream
     * @param saveTo  SaveFormat {@link com.aspose.words.SaveFormat }
     */
    public  void conversion(InputStream inputStream, OutputStream outputStream, int saveTo ) {
        // 验证License 若不验证则转化出的pdf文档会有水印产生
        if (!getLicense()) {
            return;
        }
        try {
            // Address是将要被转化的word文档
            Document doc = new Document(inputStream);
            // 全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
            doc.save(outputStream, saveTo);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            throw ParameterRuntimeException.throwException("word转pdf失败");
        }
    }
    public  boolean getLicense() {
        boolean result = false;
        try {
            // license.xml应放在..\WebRoot\WEB-INF\classes路径下
            InputStream is = this.getClass().getClassLoader().getResourceAsStream("license.xml");
            License aposeLic = new License();
            aposeLic.setLicense(is);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
