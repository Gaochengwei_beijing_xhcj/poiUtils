package com.xiaominge.utils.wordUtils.wordInputUtils;

import java.util.List;

/**
 * @program: wordtest
 * @description: word读取的内容
 * @author: xiaominge
 * @create: 2019-06-20 09:22
 **/

public class WordInfoBean {
    /**
     * 文件类型 1-doc 2-docx
     */
    private int fileType;


    /**
     * 文件名
     */
    private String fileName;
    /**
     * 文件保存的图片
     */
    private List<String> imagePath;
    /**
     * 文件内容  文本格式
     */
    private String ContextText;
    /**
     * 转成的html格式
     */
    private String ContextHtml;

    public int getFileType() {
        return fileType;
    }

    public void setFileType(int fileType) {
        this.fileType = fileType;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public List<String> getImagePath() {
        return imagePath;
    }

    public void setImagePath(List<String> imagePath) {
        this.imagePath = imagePath;
    }

    public String getContextText() {
        return ContextText;
    }

    public void setContextText(String contextText) {
        ContextText = contextText;
    }

    public String getContextHtml() {
        return ContextHtml;
    }

    public void setContextHtml(String contextHtml) {
        ContextHtml = contextHtml;
    }

    public String getFileName() {
        return fileName;
    }

    @Override
    public String toString() {
        return "WordInfoBean{" +
                "fileType=" + fileType +
                ", fileName='" + fileName + '\'' +
                ", imagePath=" + imagePath +
                ", ContextText='" + ContextText + '\'' +
                ", ContextHtml='" + ContextHtml + '\'' +
                '}';
    }

}
