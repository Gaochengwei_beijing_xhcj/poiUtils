package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.tableBean;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @program: data_centre_java
 * @description:
 * @author: Administrator
 * @create: 2020-07-22 14:32
 **/

public class OneRow {

    List<OneCell> cells;


    public OneRow(List<OneCell> cells) {
        this.cells = cells;
    }

    public OneRow() {
        this.cells = new ArrayList<>();
    }

    //放入cell
    public OneRow putOneCell(OneCell oneCell) {
        this.cells.add(oneCell);
        return this;
    }

    public OneCell createCell(String cellKey, Object cellValue) {
        OneCell cell = new OneCell(cellKey, cellValue);
        this.cells.add(cell);
        return cell;
    }

    public OneCell createCell(String cellKey, Object cellValue, String defaultValue) {
        OneCell cell;
        if (Objects.isNull(cellValue)) {
            cell = new OneCell(cellKey, defaultValue);
        } else {
            cell = new OneCell(cellKey, cellValue);
        }
        this.cells.add(cell);
        return cell;
    }

    public OneCell createOneCell(String key, String imageType, InputStream imageInputStream, int width, int height) {
        OneCell cell = new OneCell(key, imageType, imageInputStream, width, height);
        this.cells.add(cell);
        return cell;
    }


    public void clear() {
        this.cells.clear();
    }

    public List<OneCell> getCells() {
        return cells;
    }

}
