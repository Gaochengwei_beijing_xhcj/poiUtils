package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean;

import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.tableBean.TableBeans;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.textBean.Text;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.textBean.TextFroReaders;
import lombok.Setter;

/**
 * @program: data_centre_java
 * @description: 需要循环的渲染的段落文本
 * @author: Administrator
 * @create: 2020-07-22 10:04
 **/

@Setter
public class WordInfo {
    private String twoSpace = "    ";
    private String fourSpace = "        ";

    //普通文字 直接渲染
    private Text text;

    //有段落循环渲染
    private TextFroReaders textFroReaders;


    // 有多个表格信息
    private TableBeans tableBeans;

    //页脚里面的内容  页脚里面不支持 循环  但是支持图片
    private Text footInfos;

    public WordInfo() {
    }

    /**
     *
     * @return 创建一个段落替换对象
     */
    public Text createText() {
        if (text == null) {
            synchronized (this) {
                if (text == null) {
                    Text text = new Text();
                    this.text = text;
                }
                return text;
            }
        }
        return text;
    }

    public Text createFootInfos() {
        if (footInfos == null) {
            synchronized (this) {
                if (footInfos == null) {
                    Text footInfos = new Text();
                    this.footInfos = footInfos;
                }
                return footInfos;
            }
        }
        return footInfos;
    }

    /**
     *
     * @return 创建一个段落循环 容器 里面都是需要循环的替换值的
     */
    public TextFroReaders createTextFroReaders() {

        if (this.textFroReaders == null) {
            synchronized (this) {
                if (this.textFroReaders == null) {
                    TextFroReaders textFroReaders = new TextFroReaders();
                    this.textFroReaders = textFroReaders;
                }
                return textFroReaders;
            }
        }

        return textFroReaders;
    }

    public TableBeans createTableBean() {
        if (tableBeans == null) {
            synchronized (this) {
                if (tableBeans == null) {
                    TableBeans tableBeans = new TableBeans();
                    this.tableBeans = tableBeans;
                }
                return tableBeans;
            }
        }
        return tableBeans;
    }

    public Text getText() {
        if (text == null) {
            return createText();
        }
        return text;
    }

    public TextFroReaders getTextFroReaders() {
        if (textFroReaders == null) {
            return createTextFroReaders();
        }
        return textFroReaders;
    }

    public TableBeans getTableBeans() {
        if (tableBeans == null) {
            return createTableBean();
        }
        return tableBeans;
    }

    public Text getFootInfos() {
        if (footInfos == null) {
            return createFootInfos();
        }
        return footInfos;
    }

    //获取空格
    public String getTwoSpace() {
        return twoSpace;
    }

    public String getFourSpace() {
        return fourSpace;
    }
}
