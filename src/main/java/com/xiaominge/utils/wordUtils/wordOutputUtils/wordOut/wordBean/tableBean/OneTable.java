package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.tableBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: data_centre_java
 * @description: 一个表格
 * @author: Administrator
 * @create: 2020-07-22 15:13
 **/

//一个表格对象
public class OneTable {
    //表格的key
    private final String tableKey;

    private final TableType tableType;

    //表格的行
    private final List<OneRow> rows;

    //表格的标题行替换
    private final OneRow tableTitleRow;

    //表格尾行
    private final OneRow tableEndRow;

    //构造
    public OneTable(String tableKey, TableType tableType) {
        this.tableType = tableType;
        this.tableKey = tableKey;
        this.rows = new ArrayList<>();
        this.tableTitleRow = new OneRow();
        this.tableEndRow = new OneRow();
    }


    //清空方法
    public void clear() {
        this.rows.clear();
    }

    //添加一行数据
    public OneTable putOneRow(OneRow oneRow) {
        this.rows.add(oneRow);
        return this;
    }

    //创建一行数据
    public OneRow createNewOneRow() {
        OneRow row = new OneRow();
        this.rows.add(row);
        return row;
    }

    //创建标题
    public OneRow createTitleRow() {
        return this.tableTitleRow;
    }

    //创建尾部行
    public OneRow createEndRow() {
        return this.tableEndRow;
    }

    //表格的类型
    public TableType getTableType() {
        return tableType;
    }

    //获取table 的key
    public String getTableKey() {
        return this.tableKey;
    }

    //获取表格所有的行
    public List<OneRow> getRows() {
        return rows;
    }

    //获取表格标题
    public OneRow getTableTitleRow() {
        return tableTitleRow;
    }

    //获取表格尾行
    public OneRow getTableEndRow() {
        return tableEndRow;
    }
}