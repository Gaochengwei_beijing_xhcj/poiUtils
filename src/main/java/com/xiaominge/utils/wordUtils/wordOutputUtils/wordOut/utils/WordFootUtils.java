package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.utils;

import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.utils.operationUtils.RunReplaceUtils;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.ParameterBean;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.textBean.Text;
import lombok.SneakyThrows;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: data_centre_java
 * @description: word 段落处理工具
 * @author: Administrator
 * @create: 2020-07-23 17:56
 **/

public class WordFootUtils {

    //word 操作对象
    private XWPFDocument document;


    public WordFootUtils(XWPFDocument document) {
        this.document = document;
    }

    /**
     * 根据 模板段落 和 数据 在文档末尾生成段落list
     *
     * @param templateParagraphs      模板段落集合
     * @param templateParagraphRemove 需要删除的段落集合
     * @param oneForTextList          参数集合
     * @author Juveniless
     * @date 2017年11月27日 上午11:49:42
     */
    public void addParagraphInDocFooter(List<XWPFParagraph> templateParagraphs,
                                        List<XWPFParagraph> templateParagraphRemove,
                                        List<Text> oneForTextList) {

        //删除
        for (XWPFParagraph xwpfParagraph : templateParagraphRemove) {
            int beginRunIndex = xwpfParagraph.searchText("##{", new PositionInParagraph()).getBeginRun();// 标签开始run位置
            int endRunIndex = xwpfParagraph.searchText("}##", new PositionInParagraph()).getEndRun();// 结束标签
            List<XWPFRun> runs = xwpfParagraph.getRuns();
            ArrayList<Integer> removeRunList = new ArrayList<>();//需要移除的run
            //处理中间的run
            for (int i = beginRunIndex + 1; i < endRunIndex; i++) {
                XWPFRun run = runs.get(i);
                String runText = run.toString();
                removeRunList.add(i);
            }
            //处理中间的run标签
            for (int i = 0; i < removeRunList.size(); i++) {
                XWPFRun xWPFRun = runs.get(removeRunList.get(i));//原始run
                XWPFRun insertNewRun = xwpfParagraph.insertNewRun(removeRunList.get(i));
                insertNewRun.getCTR().setRPr(xWPFRun.getCTR().getRPr());
                insertNewRun.setText("");
                xwpfParagraph.removeRun(removeRunList.get(i) + 1);//移除原始的run
            }
        }

        for (Text oneForText : oneForTextList) {

            List<ParameterBean> texts = oneForText.getTexts();

            for (XWPFParagraph templateParagraph : templateParagraphs) {
                //创建段落对象
                XWPFParagraph createParagraph = document.createParagraph();
                // 设置段落样式
                createParagraph.getCTP().setPPr(templateParagraph.getCTP().getPPr());
                // 移除原始内容
                for (int pos = 0; pos < createParagraph.getRuns().size(); pos++) {
                    createParagraph.removeRun(pos);
                }
                // 添加Run标签
                for (XWPFRun s : templateParagraph.getRuns()) {
                    XWPFRun targetrun = createParagraph.createRun();
                    copyRun(targetrun, s);
                }

                //根据map替换段落元素内的{**}标签
                RunReplaceUtils.replaceParagraph(createParagraph, texts);
            }
        }

    }


    /**
     * 根据 模板段落 和 数据 在文档末尾生成段落
     *
     * @param templateParagraph 模板段落
     * @param text              不循环数据集
     * @author Juveniless
     * @date 2017年11月27日 上午11:49:42
     */
    public void addParagraphInDocFooter(XWPFParagraph templateParagraph,
                                        Text text) {

        RunReplaceUtils.replaceParagraph(templateParagraph, text.getTexts());
    }

    /**
     * 复制文本节点run
     *
     * @param newRun      新创建的的文本节点
     * @param templateRun 模板文本节点
     * @author Juveniless
     * @date 2017年11月27日 下午3:47:17
     */
    @SneakyThrows
    private void copyRun(XWPFRun newRun, XWPFRun templateRun) {
        newRun.getCTR().setRPr(templateRun.getCTR().getRPr());
        // 设置文本
        newRun.setText(templateRun.toString());
        //复制图片
        List<XWPFPicture> embeddedPictures = templateRun.getEmbeddedPictures();
        if (embeddedPictures != null) {
            for (XWPFPicture embeddedPicture : embeddedPictures) {
                newRun.addPicture(embeddedPicture.getPictureData().getPackagePart().getInputStream(), embeddedPicture.getPictureData().getPictureType(),
                        embeddedPicture.getPictureData().getFileName(), Units.toEMU(embeddedPicture.getWidth()),Units.toEMU(embeddedPicture.getDepth()));
            }
        }


    }


}
