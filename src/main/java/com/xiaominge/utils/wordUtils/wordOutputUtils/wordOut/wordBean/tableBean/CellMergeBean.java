package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.tableBean;

import com.xiaominge.exception.ParameterRuntimeException;
import lombok.Getter;
import lombok.Setter;

/**
 * @program: data_centre_java
 * @description: word表格合并对象
 * @author: Administrator
 * @create: 2020-07-22 11:53
 **/
@Getter
@Setter
public class CellMergeBean {
    /***
     * 开始行
     */
    private int startRow;
    /**
     * 结束行
     */
    private int endRow;
    /**
     * 开始列   开始从 1
     */
    private int startCol;
    /**
     * 结束列
     */
    private int endCol;

    /**
     * 合并多少行  0-代表不合并
     */
    private int mergeNum;


    //构造 合并行 和列的构造
    private CellMergeBean(int mergeNum, int startCol, int endCol) {
        if (mergeNum > 0) {
            this.mergeNum = mergeNum - 1;  //减去1 目的是为了 去除当前行  数据为1行的时候 不要进行合并   合并的时候 好计算

        } else if (mergeNum == 0) {
            this.mergeNum = mergeNum;
        } else {
            throw ParameterRuntimeException.throwException("合并的行数不能小于0");
        }
        if (startCol <= 0) {
            throw ParameterRuntimeException.throwException("开始列,列序号从1开始");
        }
        this.startCol = startCol - 1;


        if (endCol - 1 < this.startCol) {
            throw ParameterRuntimeException.throwException("结束列序号不能比开始列小");
        }
        this.endCol = endCol - 1;
    }


    //合并 列
    public static CellMergeBean CreateColMergeBean(int startCol, int endCol) {
        CellMergeBean cellMergeBean = new CellMergeBean(0, startCol, endCol);
        return cellMergeBean;
    }

    //行 和列 都合并
    public static CellMergeBean CreateRowAndColMergeBean(int mergeNum, int startCol, int endCol) {
        CellMergeBean cellMergeBean = new CellMergeBean(mergeNum, startCol, endCol);
        return cellMergeBean;
    }

    //行合并
    public static CellMergeBean CreateRowMergeBean(int mergeNum, int startCol) {
        CellMergeBean cellMergeBean = new CellMergeBean(mergeNum, startCol, startCol);
        return cellMergeBean;
    }


}
