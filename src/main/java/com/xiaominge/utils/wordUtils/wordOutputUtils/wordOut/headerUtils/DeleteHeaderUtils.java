package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.headerUtils;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFHeader;

import java.util.List;

/**
 * @program: system_platform
 * @description: 删除页脚
 * @author: xiaominge
 * @create: 2021-01-08 14:55
 **/

public class DeleteHeaderUtils {
    //删除所有默认的页眉
    public static void deleteHeader(XWPFDocument docx) {
        List<XWPFHeader> headers = docx.getHeaderList();

        for (XWPFHeader header : headers) {
            header.setHeaderFooter(org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHdrFtr.Factory.newInstance());

        }
    }

}
