package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.utils;

import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.WordInfo;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.tableBean.OneTable;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.tableBean.TableBeans;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.textBean.Text;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.wp.usermodel.Paragraph;
import org.apache.poi.xwpf.usermodel.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: data_centre_java
 * @description: word 模板工具
 * @author: Administrator
 * @create: 2020-07-22 17:22
 **/

@Slf4j
public class WordTemplateUtils {

    private XWPFDocument document;

    private WordTableUtils wordTableUtils;
    private WordRunUtils wordRunUtils;
    private WordFootUtils wordFootUtils;

    /**
     * @param inputStream 模板流
     *                    //构造方法
     * @throws IOException
     */
    public WordTemplateUtils(InputStream inputStream) throws IOException {
        this.document = new XWPFDocument(inputStream);
        this.wordTableUtils = new WordTableUtils(document);
        this.wordRunUtils = new WordRunUtils(document);
        this.wordFootUtils = new WordFootUtils(document);
    }

    /**
     * 输出方法
     *
     * @param outputStream 输出流
     * @throws IOException
     */
    public void write(OutputStream outputStream) throws IOException {
        document.write(outputStream);
        outputStream.close();
    }

    /**
     * 直接将文件相应给界面
     *
     * @param response
     * @param RealFileNme 文件
     * @param outFileName 相应的文名称
     * @throws Exception
     */

    public void httpwrite(HttpServletResponse response, String RealFileNme, String outFileName) throws Exception {
        //设置输出的类型
        response.setContentType("application/doc"); // 设置返回内容格式
        response.setHeader("Content-disposition", "attachment; filename=" + URLEncoder.encode(outFileName, "utf-8"));
        ServletOutputStream outputStream = response.getOutputStream();

        //读取文件
        File file = new File(RealFileNme);
        FileInputStream fileInputStream = new FileInputStream(file);//获取输出的文件
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);

        try {
            byte[] b = new byte[1024 * 10];   //代表一次最多读取10KB的内容
            int length = 0; //代表实际读取的字节数
            while ((length = bufferedInputStream.read(b)) != -1) {
                //length 代表实际读取的字节数
                outputStream.write(b, 0, length);
            }
            //缓冲区的内容写入到文件
            outputStream.flush();

            //赋值
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //document.close();
            outputStream.close();
            bufferedInputStream.close();
            fileInputStream.close();
            //输出完成之后 删除文件
            file.delete();

        }

    }


    /**
     *
     * @param wordInfo 需要替换的数据
     * @return
     */
    @SneakyThrows
    public XWPFDocument replaceDocument(WordInfo wordInfo) {
        // 所有对象（段落+表格）
        List<IBodyElement> bodyElements = document.getBodyElements();
        // 标记模板文件（段落+表格）总个数
        int templateBodySize = bodyElements.size();
        //判断是否有替换的数据
        if (null == wordInfo) {
            // 没有数据返回空word ，
            log.error("数据源错误--数据源缺失");
            // 删除文本中的模板内容
            for (int a = 0; a < templateBodySize; a++) {
                document.removeBodyElement(0);
            }
            return document;
        }
        //获取替换的参数
        TableBeans tableBeans = wordInfo.getTableBeans();

        int curT = 0;// 当前操作表格对象的索引
        int curP = 0;// 当前操作段落对象的索引
        for (int i = 0; i < templateBodySize; i++) {
            //循环获取当前的节点信息
            IBodyElement body = bodyElements.get(i);
            //判断是表格 还是文字
            if (BodyElementType.TABLE.equals(body.getElementType())) { // 处理表格

                List<XWPFTable> tables = body.getBody().getTables();
                //获取当前循环到的表格
                XWPFTable table = tables.get(curT);
                if (table != null) {
                    List<XWPFTableCell> tableCells = table.getRows().get(0).getTableCells();// 获取到模板表格第一行，用来获取表格名称
                    if (tableCells.size() != 1 || tableCells.get(0).getText().length() == 0) {
                        log.error("表格第一行只能有1列,只能设置表格key");
                        continue;
                        //return;

                    }
                    String templateTableName = tableCells.get(0).getText();
                    if (!tableBeans.containsKey(templateTableName)) { //后期在这这里删除模板里面的表格
                        log.info("文档中第" + (curT + 1) + "个表格模板数据源缺失,跳过渲染该表格-->{}", templateTableName);
                        continue;
                        //return;
                    }
                    OneTable oneTable = tableBeans.getOneTable(templateTableName);
                    switch (oneTable.getTableType()) {
                        case for_table:
                            //log.info("循环生成表格");
                            wordTableUtils.addTableInDocFooter(table, tableBeans.getOneTable(templateTableName), 1);
                            break;
                        case for_Row:
                            //log.info("循环生成表格内部的行");
                            wordTableUtils.addTableInDocFooter(table, tableBeans.getOneTable(templateTableName), 2);
                            break;
                        case replace_value:
                            //log.info("简单替换标签");
                            wordTableUtils.addTableInDocFooter(table, tableBeans.getOneTable(templateTableName), 3);
                            break;
                        case No_todo:
                            wordTableUtils.addTableInDocFooter(table, null, 0);
                            break;
                        default:
                            log.info("表格处理方法错误");

                            return document;
                    }
                    curT++;

                }
                //段落文字
            } else if (BodyElementType.PARAGRAPH.equals(body.getElementType())) {  // 处理段落
                //log.info("获取到段落");
                //获取处理的段落信息
                XWPFParagraph ph = body.getBody().getParagraphArray(curP);

                if (ph != null) {
                    //自己封装 段落循环
                    List<XWPFRun> runs = ph.getRuns();
                    StringBuffer value = new StringBuffer();
                    for (int i1 = 0; i1 < runs.size(); i1++) {
                        String wordText = runs.get(i1).getText(0);
                        value.append(wordText);
                    }


                    List<XWPFParagraph> removeList = new ArrayList<>(); //删除的段落
                    List<XWPFParagraph> addList = new ArrayList<>();   //新增的段落

                    // List<Map<String, WordValueBeanAndMergeCell>> maps = null;

                    List<Text> oneForTextList = null; //一个段落循环的数据里面的数据
                    String keyCode = WordTemplateKeyEnum.textForStart.getKeyCode();
                    if (value.toString().trim().startsWith(keyCode)) { //开始
                        //段落循环
                        removeList.add(ph);
                        //循环获取当前的节点信息
                        i++;
                        while (true) {
                            curP++;
                            //循环获取当前的节点信息
                            IBodyElement bodytemp = bodyElements.get(i);
                            if (BodyElementType.PARAGRAPH.equals(bodytemp.getElementType())) { // 处理段落
                                XWPFParagraph phtemp = bodytemp.getBody().getParagraphArray(curP);
                                //自己封装
                                List<XWPFRun> runstemp = phtemp.getRuns();
                                StringBuffer valuetemp = new StringBuffer();

                                for (XWPFRun run : runstemp) {
                                    String runText = run.getText(0);
                                    valuetemp.append(runText);
                                }
                                if (valuetemp.toString().trim().startsWith(WordTemplateKeyEnum.textForEnd.getKeyCode())) {//段落循环结束处理
                                    removeList.add(phtemp);
                                    break;
                                } else if (valuetemp.toString().trim().startsWith(WordTemplateKeyEnum.textForKeyPrefix.getKeyCode())) {//获取表格数据
                                    String forEachKey = valuetemp.substring(valuetemp.indexOf(WordTemplateKeyEnum.textForKeyPrefixValue.getKeyCode()),
                                            valuetemp.indexOf(WordTemplateKeyEnum.keyEnd.getKeyCode())).replace(WordTemplateKeyEnum.textForKeyPrefixValue.getKeyCode(), "");

                                    oneForTextList = wordInfo.getTextFroReaders().getOneForTextList(forEachKey).getOneForTextList();

                                    if (oneForTextList == null) {
                                        throw new RuntimeException("缺少元数据" + valuetemp);
                                    }
                                } else {//都不是就是渲染的数据
                                    addList.add(phtemp);
                                }
                            } else {
                                throw new RuntimeException("##{forEachRunstart 只适合文本循环");
                            }
                            i++;
                        }
                        //渲染数据
                        if (oneForTextList == null) {
                            throw new RuntimeException("缺少元数据文字循环");
                        }
                        //段落循环
                        wordRunUtils.addParagraphInDocFooter(addList, removeList, oneForTextList);
                    } else {
                        //直接渲染
                        Text wordInfoText = wordInfo.getText();
                        wordRunUtils.addParagraphInDocFooter(ph, wordInfoText);
                    }
                    curP++;
                }

            }

        }
        //获取所有页脚
        List<XWPFFooter> footerList = document.getFooterList();
        //标记页脚个数
        int footSize = footerList.size();
        for (int i = 0; i < footSize; i++) {
            int footCurp = 0;
            XWPFFooter oneFoot = footerList.get(i);
            List<IBodyElement> footBodyList = oneFoot.getBodyElements();

            for (int j = 0; j < footBodyList.size(); j++) {
                IBodyElement oneBody = footBodyList.get(j);
                if (BodyElementType.PARAGRAPH.equals(oneBody.getElementType())) {
                    XWPFParagraph ph = oneBody.getBody().getParagraphArray(footCurp);
                    //直接渲染
                    Text wordInfoText = wordInfo.getFootInfos();

                    wordFootUtils.addParagraphInDocFooter(ph, wordInfoText);
                    footCurp++;
                }
            }
        }

      //   处理完毕模板，删除文本中的模板内容
         for (int a = 0; a < templateBodySize; a++) {
           document.removeBodyElement(0);
          }


        return document;
    }


}
