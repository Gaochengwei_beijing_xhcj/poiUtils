package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.footUtils;

import com.xiaominge.exception.ParameterRuntimeException;
import com.xiaominge.utils.IdUtils;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.FontBean;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.FootBeans.FootPageBean;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.ParameterBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: system_platform
 * @description: 创建页脚
 * @author: xiaominge
 * @create: 2021-01-04 09:12
 **/

public class CreateFootUtils {


    /**
     * 在内容中生成的内容 假装是页脚 这是为了自己写入分页
     * 如果需要每页都有页脚信息  可以在模板中设置页脚
     *
     * @param document
     * @param fontBean       字体对象
     * @param parameterBeans 页脚内容
     * @throws Exception
     */
    public static void createFooter(XWPFDocument document, FontBean fontBean, List<ParameterBean> parameterBeans, FootPageBean footPageBean) throws IOException, XmlException {
        /*
         * 生成页脚段落
         * */
        CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
        XWPFHeaderFooterPolicy headerFooterPolicy = new XWPFHeaderFooterPolicy(document, sectPr);
        XWPFFooter footer = headerFooterPolicy.createFooter(STHdrFtr.DEFAULT);
        XWPFParagraph paragraph = footer.getParagraphArray(0);
        if (paragraph == null) {
            paragraph = footer.createParagraph();
        }

        paragraph.setAlignment(ParagraphAlignment.CENTER);
        paragraph.setVerticalAlignment(TextAlignment.CENTER);

        //paragraph.setBorderTop(Borders.THICK); //设置页脚边框

        CTTabStop tabStop = paragraph.getCTP().getPPr().addNewTabs().addNewTab();
        tabStop.setVal(STTabJc.RIGHT);
        int twipsPerInch = 1440;
        tabStop.setPos(BigInteger.valueOf(6 * twipsPerInch));


        if (parameterBeans != null && !parameterBeans.isEmpty()) {
            XWPFRun run = paragraph.createRun();
            String info = parameterBeans.stream().filter(parameterBean -> null != parameterBean.getValue()).map(ParameterBean::getValue).map(o -> o.toString()).collect(Collectors.joining());
            run.setText(info);
            List<ParameterBean> imageInfos = parameterBeans.stream().filter(parameterBean -> null != parameterBean.getImageInputStream()).collect(Collectors.toList());

            for (ParameterBean imageInfo : imageInfos) {
                try {
                    XWPFPicture picture = run.addPicture(imageInfo.getImageInputStream(),
                            imageInfo.getImageTypeCode(),
                            imageInfo.getValue() == null ? IdUtils.getId() : imageInfo.getValue().toString(),
                            Units.toEMU(imageInfo.getWidth()),
                            Units.toEMU(imageInfo.getHeight())
                    );
                    String blipID = "";
                    for (XWPFPictureData picturedata : footer.getAllPackagePictures()) {
                        blipID = footer.getRelationId(picturedata);
                    }
                    picture.getCTPicture().getBlipFill().getBlip().setEmbed(blipID);
                    run.addBreak();
                    imageInfo.getImageInputStream().close();
                } catch (Exception e) {
                    e.printStackTrace();
                    ParameterRuntimeException.throwException("页脚图片写入失败");
                }

            }
            setXWPFRunStyle(run, fontBean);
            run.addBreak();
        }


        if (footPageBean != null) {
            /*
             * 生成页码
             * 页码右对齐
             * */
            XWPFRun run = paragraph.createRun();
            FontBean pageFontBean = footPageBean.getFontBean();
            run.setText(footPageBean.getStartPageContent());
            setXWPFRunStyle(run, pageFontBean);

            run = paragraph.createRun();
            CTFldChar fldChar = run.getCTR().addNewFldChar();
            fldChar.setFldCharType(STFldCharType.BEGIN);

            run = paragraph.createRun();
            CTText ctText = run.getCTR().addNewInstrText();
            ctText.setStringValue("PAGE  \\* MERGEFORMAT");
            ctText.setSpace(SpaceAttribute.Space.PRESERVE);
            setXWPFRunStyle(run, pageFontBean);

            fldChar = run.getCTR().addNewFldChar();
            fldChar.setFldCharType(STFldCharType.END);

            String pageConcatContent = footPageBean.getPageConcatContent();
            if (StringUtils.isNotBlank(pageConcatContent)) {
                run = paragraph.createRun();
                run.setText(pageConcatContent);
                setXWPFRunStyle(run, pageFontBean);

                run = paragraph.createRun();
                fldChar = run.getCTR().addNewFldChar();
                fldChar.setFldCharType(STFldCharType.BEGIN);

                run = paragraph.createRun();
                ctText = run.getCTR().addNewInstrText();
                ctText.setStringValue("NUMPAGES  \\* MERGEFORMAT ");
                ctText.setSpace(SpaceAttribute.Space.PRESERVE);
                setXWPFRunStyle(run, pageFontBean);

                fldChar = run.getCTR().addNewFldChar();
                fldChar.setFldCharType(STFldCharType.END);
                run = paragraph.createRun();
                run.setText(footPageBean.getEndContent());
                setXWPFRunStyle(run, pageFontBean);
            }
        }
    }


    //创建默认的页码
    public static void createDefaultFooter(XWPFDocument docx, FontBean fontBean) throws IOException, XmlException {
        XWPFParagraph paragraph = docx.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun run = paragraph.createRun();
        CTFldChar fldChar = run.getCTR().addNewFldChar();
        fldChar.setFldCharType(STFldCharType.BEGIN);
        run.setFontFamily(fontBean.getFontName());
        run.setFontSize(fontBean.getFontSize());//小五

        CTText ctText = run.getCTR().addNewInstrText();
        ctText.setStringValue(" PAGE \\ MERGEFORMAT ");
        ctText.setSpace(SpaceAttribute.Space.DEFAULT);
        fldChar = run.getCTR().addNewFldChar();
        fldChar.setFldCharType(STFldCharType.END);
        CTSectPr sectPr = paragraph.getCTP().getPPr().addNewSectPr();

        XWPFHeaderFooterPolicy policy = new XWPFHeaderFooterPolicy(docx, sectPr);
        XWPFParagraph[] paragraphs = new XWPFParagraph[1];
        paragraphs[0] = paragraph;
        policy.createFooter(STHdrFtr.DEFAULT, paragraphs);
    }


    /**
     * 设置字体样式
     *
     * @param xwpfRun 段落元素
     */
    private static void setXWPFRunStyle(XWPFRun xwpfRun, FontBean fontBean) {
        xwpfRun.setFontSize(fontBean.getFontSize());
        CTRPr rpr = xwpfRun.getCTR().isSetRPr() ? xwpfRun.getCTR().getRPr() : xwpfRun.getCTR().addNewRPr();
        CTFonts fonts = rpr.sizeOfRFontsArray()!=0 ? rpr.getRFontsArray(rpr.getRFontsArray().length-1) : rpr.addNewRFonts();
        fonts.setAscii(fontBean.getFontName());
        fonts.setEastAsia(fontBean.getFontName());
        fonts.setHAnsi(fontBean.getFontName());
    }

}
