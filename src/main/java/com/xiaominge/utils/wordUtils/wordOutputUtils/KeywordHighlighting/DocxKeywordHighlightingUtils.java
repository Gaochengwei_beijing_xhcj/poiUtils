package com.xiaominge.utils.wordUtils.wordOutputUtils.KeywordHighlighting;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTShd;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STShd;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * 关键字高亮显示
 * 只支持docx
 *
 * @author xiaominge
 * @date 2023/05/30
 */
public class DocxKeywordHighlightingUtils {
    /**
     * 做高照明
     *
     * @param inputStream  输入流
     * @param outputStream 输出流
     * @param data         数据
     * @param fontColor    字体颜色  RRGGBB
     * @param bgColor      bg颜色  RRGGBB
     */
    public static void doHighLighting(InputStream inputStream, OutputStream outputStream, List<String> data, String fontColor, String bgColor) {
        try {
            XWPFDocument xdoc = new XWPFDocument(inputStream);
            if (StringUtils.isBlank(fontColor)) {
                fontColor = "ff0000";//默认红色
            }
            if (StringUtils.isBlank(bgColor)) {
                bgColor = "ffff00";//默认黄底
            }

            for (int i = 0; i < xdoc.getParagraphs().size(); i++) {
                XWPFParagraph xwpfParagraph = xdoc.getParagraphs().get(i);

                String text1 = xwpfParagraph.getText();
                if (data != null) {
                    for (String datum : data) {
                        if (text1.contains(datum)) {
                            // 取出纯文本
                            String text = xwpfParagraph.getText();
                            // ---------- 拆分段落为：前，关键字，后 三个run ----------
                            String begin = text.substring(0, text.indexOf(datum));
                            xwpfParagraph.getRuns().get(0).getParagraph().createRun().setText(begin);// 取出段落中关键字之前的内容
                            String end = text.substring(text.indexOf(datum) + datum.length());    // 取出段落中关键字之后的内容
                            XWPFRun run = xwpfParagraph.getRuns().get(0).getParagraph().createRun();    // 创建 run 用来替换原内容
                            // 替换文字
                            run.setColor(fontColor);    // 文字颜色
                            run.setBold(true);            // 设置加粗
                            run.setText(datum);        // 文字内容
                            // 设置背景色
                            if (bgColor != null) {
                                CTShd cTShd = run.getCTR().addNewRPr().addNewShd();    // 向run中添加一个 cTShd，应该就是对应vba中的shading对象
                                cTShd.setVal(STShd.CLEAR);
                                cTShd.setColor("auto");
                                cTShd.setFill(bgColor);
                            }
                            // ---------- 拼回段落 ----------
                            xwpfParagraph.getRuns().get(0).getParagraph().createRun().setText(end);
                            xwpfParagraph.getRuns().get(0).getParagraph().removeRun(0);
                        }
                    }
                }
            }
            xdoc.write(outputStream);
            inputStream.close();
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
