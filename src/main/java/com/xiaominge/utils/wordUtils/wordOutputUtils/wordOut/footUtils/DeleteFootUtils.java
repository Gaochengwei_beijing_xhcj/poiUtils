package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.footUtils;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFFooter;

import java.util.List;

/**
 * @program: system_platform
 * @description: 删除页脚
 * @author: xiaominge
 * @create: 2021-01-08 14:55
 **/

public class DeleteFootUtils {
    //删除所有默认的页脚
    public static void deleteFoot(XWPFDocument docx) {
        List<XWPFFooter> footers = docx.getFooterList();
        for (XWPFFooter footer : footers) {
            footer.setHeaderFooter(org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHdrFtr.Factory.newInstance());

        }

    }
}
