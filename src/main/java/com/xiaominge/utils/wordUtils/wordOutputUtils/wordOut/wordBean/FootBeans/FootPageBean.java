package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.FootBeans;

import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.FontBean;
import lombok.Getter;
import lombok.Setter;

/**
 * @program: system_platform
 * @description: 页脚页码信息
 * @author: xiaominge
 * @create: 2021-01-04 10:17
 **/

@Getter
@Setter
public class FootPageBean {
    /**
     * 分页开始文字  第
     */
    private String startPageContent = "";

    /**
     * 分页之间 连接文字       页   共
     */
    private String pageConcatContent;

    /**
     * 结尾 信息  end  页
     */
    private String endContent = "";


    private FontBean fontBean = new FontBean("黑体", 9);

    public FootPageBean(String startPageContent, String pageConcatContent, String endContent, FontBean fontBean) {
        this.startPageContent = startPageContent;
        this.pageConcatContent = pageConcatContent;
        this.endContent = endContent;
        if (fontBean != null) {
            this.fontBean = fontBean;
        }
    }


    public FootPageBean(String startPageContent, String pageConcatContent, String endContent) {
        this.startPageContent = startPageContent;
        this.pageConcatContent = pageConcatContent;
        this.endContent = endContent;
    }
}
