package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean;

import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.utils.operationUtils.WordValueEnum;
import lombok.Getter;
import lombok.Setter;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.io.InputStream;

/**
 * @program: data_centre_java
 * @description: word 渲染数据的对应对象
 * @author: Administrator
 * @create: 2020-07-22 10:02
 **/

@Getter
@Setter
public class ParameterBean {


    // 替换的key
    private String key;
    /**
     * 替换的value值
     * 自定义逻辑 如果值中间有<word_underline>xxxxxxx</word_underline> 则会加下划线 默认样式
     * 自定义逻辑 \n 将则会换行
     * {@link com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.utils.operationUtils.RunReplaceUtils#setValue(ParameterBean, XWPFRun, String)}
     * {@link  WordValueEnum }
     */
    private Object value;

    /**
     * html线下下划线 设置默认值
     */
    private UnderlinePatterns underlinePatternsValueUnderLineHtml = UnderlinePatterns.SINGLE;

    /**
     * 是否加粗 默认根据模版的花括号样式来
     * true 为加粗 false 为不加粗
     * null 为不修改 根据模版
     */
    private Boolean bold;

    /**
     * 下划线图案
     * DASH_LONG
     */
    private UnderlinePatterns underlinePatterns;

    //十六进制的颜色值 默认黑色
    private String UnderlineColor = "000000";


    /**
     * 图片的类型
     */
    private String imageType;
    /**
     * 图片类型code
     */
    private int imageTypeCode;

    /**
     * 图片的宽度
     */
    private int width;
    /**
     * 图片的高度
     */
    private int height;

    /**
     * 图片左右偏移量
     */
    private int leftOffset;
    /**
     * 图片上线偏移量
     */
    private int topOffset;

    /**
     * 图片流
     */
    private InputStream imageInputStream;

    /**
     * 图片是否在新的一页展示
     */
    private boolean nextIsNewPage;

    public ParameterBean(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public ParameterBean(String key, Object value, boolean nextIsNewPage) {
        this.key = key;
        this.value = value;
        this.nextIsNewPage = nextIsNewPage;
    }


    /**
     * @param key              图片的key
     * @param imageType        图片类型
     * @param imageInputStream 图片流
     * @param width            宽度
     * @param height           高度
     * @param leftOffset       左右偏移量
     * @param topOffset        上线偏移量
     * @param nextIsNewPage    是否独占一页
     */
    public ParameterBean(String key, String imageType, InputStream imageInputStream, int width, int height, int leftOffset, int topOffset, boolean nextIsNewPage) {
        if ("emf".equalsIgnoreCase(imageType)) {
            this.imageTypeCode = XWPFDocument.PICTURE_TYPE_EMF;
        } else if ("wmf".equalsIgnoreCase(imageType)) {
            this.imageTypeCode = XWPFDocument.PICTURE_TYPE_WMF;
        } else if ("pict".equalsIgnoreCase(imageType)) {
            this.imageTypeCode = XWPFDocument.PICTURE_TYPE_PICT;
        } else if ("jpeg".equalsIgnoreCase(imageType) || "jpg".equalsIgnoreCase(imageType)) {
            this.imageTypeCode = XWPFDocument.PICTURE_TYPE_JPEG;
        } else if ("png".equalsIgnoreCase(imageType)) {
            this.imageTypeCode = XWPFDocument.PICTURE_TYPE_PNG;
        } else if ("dib".equalsIgnoreCase(imageType)) {
            this.imageTypeCode = XWPFDocument.PICTURE_TYPE_DIB;
        } else if ("gif".equalsIgnoreCase(imageType)) {
            this.imageTypeCode = XWPFDocument.PICTURE_TYPE_GIF;
        } else if ("tiff".equalsIgnoreCase(imageType)) {
            this.imageTypeCode = XWPFDocument.PICTURE_TYPE_TIFF;
        } else if ("eps".equalsIgnoreCase(imageType)) {
            this.imageTypeCode = XWPFDocument.PICTURE_TYPE_EPS;
        } else if ("bmp".equalsIgnoreCase(imageType)) {
            this.imageTypeCode = XWPFDocument.PICTURE_TYPE_BMP;
        } else if ("wpg".equalsIgnoreCase(imageType)) {
            this.imageTypeCode = XWPFDocument.PICTURE_TYPE_WPG;
        } else {
            throw new RuntimeException("传入的格式:" + imageType + "图片格式不支持,暂时支持=>emf|wmf|pict|jpeg|jpg|png|dib|gif|tiff|eps|bmp|wpg ");
        }
        if (imageInputStream == null) {
            throw new RuntimeException("图片流不能为空");
        }

        this.key = key;
        this.imageType = imageType;
        this.imageInputStream = imageInputStream;
        this.height = height;
        this.width = width;
        this.leftOffset = leftOffset;
        this.topOffset = topOffset;
        this.nextIsNewPage = nextIsNewPage;
    }

    public ParameterBean(String key, String imageType, InputStream imageInputStream, int width, int height) {
        this(key, imageType, imageInputStream, width, height, 0, 0, false);
    }
}