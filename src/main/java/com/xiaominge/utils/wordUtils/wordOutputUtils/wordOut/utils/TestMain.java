package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.utils;

import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.footUtils.CreateFootUtils;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.FontBean;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.FootBeans.FootPageBean;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.ParameterBean;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.WordInfo;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.textBean.OneFor;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.textBean.Text;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.textBean.TextFroReaders;
import lombok.SneakyThrows;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @program: data_centre_java
 * @description: ceshi
 * @author: Administrator
 * @create: 2020-07-22 15:28
 **/

public class TestMain {
    @SneakyThrows
    public static void main(String[] args) throws IOException {

        WordInfo wordInfo = new WordInfo();

        //直接渲染文字
        Text text = wordInfo.createText();
        text.putValue("deptName", "测试部门名称", true);
        text.putValue("askUser", "询问人名称");

        //渲染图片
        text.putImage(
                "image_user",
                "jpg",
                new FileInputStream(new File("D:\\桌面\\老虎.jpg")),
                300,
                200,
                true
        );


        //循环渲染文字
        TextFroReaders textFroReaders = wordInfo.createTextFroReaders();

        //创建一个循环渲染的文字
        OneFor userSingFor = textFroReaders.CreateOneForText("userSing");

        //给这个循环 添加数据  第一次循环
        Text oneForText = userSingFor.createOneForText();
        oneForText.putValue("userName", "执法人员1名称");
        oneForText.putValue("cardNumber", "执法人员1执法证号");

        //第二次循环
        Text oneForText1 = userSingFor.createOneForText();
        oneForText1.putValue("userName", "执法人员2名称");
        oneForText1.putValue("cardNumber", "执法人员2执法证号");


       /* //创建所有 表格
        TableBeans tableBean = wordInfo.createTableBean();

        //创建换一个表格
        OneTable checkTypeLinkTypeMchCountTable = tableBean.CreateOneTable("checkTypeLinkTypeMchCountTable", TableType.for_Row);

        for (int i = 0; i < 20; i++) {
            OneRow oneRow = checkTypeLinkTypeMchCountTable.createNewOneRow();
            OneCell indexCell = oneRow.createCell("index", i);

            if (i==0){
                indexCell.createCellMergeBean(5,1,2 );  //从第一行 两行 两列合并

            }
            if (i==10){
                indexCell.createCellMergeBean(1,2);  //从第三行  一行 两列合并
            }

            if (i==15){ //第五航  合并一行
                indexCell.createRowMergeBean(5, 1);
            }

            oneRow.createCell("checkTypeName", "类别11" + i);

            oneRow.createCell("linkTypeName", "环节11" + i);

            oneRow.createCell("mchAllCheckNum", "抽查对象数量11" + i);

            oneRow.createCell("mchHaveProblemNum", "问题执法对象11" + i);

            oneRow.createCell("ratio", "占比11" + i);
        }
        */

/*

        //创建换一个表格
        OneTable checkTypeLinkTypeMchCountTable2 = tableBean.CreateOneTable("checkTypeLinkTypeMchCountTable1",TableType.for_table);
        for (int i = 0; i < 10; i++) {
            OneRow oneRow = checkTypeLinkTypeMchCountTable2.createNewOneRow();
            oneRow.createCell("index", i);

            oneRow.createCell("checkTypeName", "类别22" + i);

            oneRow.createCell("linkTypeName", "环节22" + i);

            oneRow.createCell("mchAllCheckNum", "抽查对象数量22" + i);

            oneRow.createCell("mchHaveProblemNum", "问题执法对象22" + i);

            oneRow.createCell("ratio", "占比22" + i);
        }
*/

/*

        OneTable checkTypeLinkTypeMchCountTable3 = tableBean.CreateOneTable("checkTypeLinkTypeMchCountTable3",TableType.replace_value);
        OneRow oneRow = checkTypeLinkTypeMchCountTable3.createEndRow();
        oneRow.createCell("index", 1);

        oneRow.createCell("checkTypeName", "类别33");

        oneRow.createCell("linkTypeName", "环节33");

        oneRow.createCell("mchAllCheckNum", "抽查对象数量333");

        oneRow.createCell("mchHaveProblemNum", "问题执法对象33");

        oneRow.createCell("ratio", "占比333");
*/


        FileInputStream fileInputStream = new FileInputStream("D:\\桌面\\aa-new.docx");

        WordTemplateUtils wordTemplateUtils = new WordTemplateUtils(fileInputStream);

        XWPFDocument xwpfDoument = wordTemplateUtils.replaceDocument(wordInfo);

        ParameterBean jpg = new ParameterBean("", "jpg", new FileInputStream((new File("D:\\桌面\\老虎.jpg"))), 50, 50);
        ArrayList<ParameterBean> list = new ArrayList<ParameterBean>() {{
            add(new ParameterBean("", "页脚1"));
            add(jpg);
        }};
        CreateFootUtils.createFooter(xwpfDoument,
                new FontBean("黑体", 11),
                null,
                new FootPageBean("", "/", "", new FontBean("Lucida Bright", 11)));

        FileOutputStream fileOutputStream = new FileOutputStream("D:\\桌面\\aa123.docx");
        wordTemplateUtils.write(fileOutputStream);

    }
}
