//package com.xiaominge.utils.wordUtils.wordOutputUtils.htmlToword;
//
//import ch.qos.logback.classic.Level;
//import ch.qos.logback.classic.Logger;
//import ch.qos.logback.classic.LoggerContext;
//import fr.opensagres.xdocreport.utils.StringEscapeUtils;
//import lombok.extern.slf4j.Slf4j;
//import org.docx4j.convert.in.xhtml.FormattingOption;
//import org.docx4j.convert.in.xhtml.XHTMLImporter;
//import org.docx4j.convert.in.xhtml.XHTMLImporterImpl;
//import org.docx4j.jaxb.Context;
//import org.docx4j.openpackaging.exceptions.Docx4JException;
//import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
//import org.docx4j.openpackaging.parts.WordprocessingML.NumberingDefinitionsPart;
//import org.docx4j.wml.RFonts;
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.jsoup.nodes.Element;
//import org.slf4j.LoggerFactory;
//
//import javax.xml.bind.JAXBException;
//import java.io.FileOutputStream;
//import java.io.OutputStream;
//import java.util.List;
//
//@Slf4j
//public class HtmlToWord {
// html 转word 格式没有完全适配 所以注释掉
//
//    /**
//     * html 转docx
//     *
//     * @param htmlStr      html str
//     * @param outputStream 输出流
//     */
//    public static void htmlToDocx(String htmlStr, OutputStream outputStream, String imagePath) throws Docx4JException, JAXBException {
//        //替换image 图片地址
//        String unescaped = replaceImage(htmlStr, imagePath);
//        if (htmlStr.contains("&lt;/")) {
//            unescaped = StringEscapeUtils.escapeHtml(unescaped);
//        }
//
//
//
//        // 创建一个空的docx对象
//        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage();
//        NumberingDefinitionsPart ndp = new NumberingDefinitionsPart();
//        wordMLPackage.getMainDocumentPart().addTargetPart(ndp);
//        ndp.unmarshalDefaultNumbering();
//
//
//        // 设置字体映射
//        RFonts rfonts = Context.getWmlObjectFactory().createRFonts();
//        rfonts.setAscii("Century Gothic");
//        XHTMLImporterImpl.addFontMapping("Century Gothic", rfonts);
//
//        // 转换XHTML，并将其添加到我们制作的空docx中
//        XHTMLImporterImpl XHTMLImporter = new XHTMLImporterImpl(wordMLPackage);
//        XHTMLImporter.setRunFormatting(FormattingOption.IGNORE_CLASS);
//        XHTMLImporter.setTableFormatting(FormattingOption.IGNORE_CLASS);
//        XHTMLImporter.setParagraphFormatting(FormattingOption.CLASS_PLUS_OTHER);
//        XHTMLImporter.setHyperlinkStyle("Hyperlink");
//
//        wordMLPackage.getMainDocumentPart().getContent().addAll(
//                XHTMLImporter.convert(unescaped, null));
//        wordMLPackage.save(outputStream);
//        wordMLPackage.clone();
//
//    }
//
//    static {
//        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
//        List<Logger> loggerList = loggerContext.getLoggerList();
//        loggerList.forEach(logger -> {
//            logger.setLevel(Level.INFO);
//        });
//    }
//
//    public static void main(String[] args) throws Exception {
//        String contextHtml = "<html><head><style>p{margin-top:0pt;margin-bottom:1pt;}p.a{text-align:justify;}p.X1{margin-top:17.0pt;margin-bottom:16.5pt;}span.X1{font-size:22.0pt;font-weight:bold;}span.X10{font-size:22.0pt;font-weight:bold;}</style></head><body><div style=\"width:595.3pt;margin-bottom:72.0pt;margin-top:72.0pt;margin-left:90.0pt;margin-right:90.0pt;\"><p style=\"text-align:center;white-space:pre-wrap;\"><br/>点对点</p><p style=\"text-align:center;white-space:pre-wrap;\"><br/></p><p style=\"text-align:center;white-space:pre-wrap;\"><br/></p><p class=\"a X1\"><span class=\"a X1\">打发大水发撒地方</span></p><p><br/></p><p>的的范德萨</p><p><br/></p><p><img src=\"123\\image1.png\" style=\"width:254.25pt;height:153.75pt;\"/></p><p><br/></p><table class=\"a1 a3\"><tr class=\"a1 a3\"><td class=\"a1 a3\" style=\"width:103.7pt;border-top:0.5px solid #000000;border-bottom:0.5px solid #000000;border-left:0.5px solid #000000;border-right:0.5px solid #000000;\"><p>1</p></td><td class=\"a1 a3\" style=\"width:103.7pt;border-top:0.5px solid #000000;border-bottom:0.5px solid #000000;border-left:0.5px solid #000000;border-right:0.5px solid #000000;\"><p>2</p></td><td class=\"a1 a3\" style=\"width:103.7pt;border-top:0.5px solid #000000;border-bottom:0.5px solid #000000;border-left:0.5px solid #000000;border-right:0.5px solid #000000;\"><p/></td><td class=\"a1 a3\" style=\"width:103.7pt;border-top:0.5px solid #000000;border-bottom:0.5px solid #000000;border-left:0.5px solid #000000;border-right:0.5px solid #000000;\"><p/></td></tr><tr class=\"a1 a3\"><td class=\"a1 a3\" style=\"width:103.7pt;border-top:0.5px solid #000000;border-bottom:0.5px solid #000000;border-left:0.5px solid #000000;border-right:0.5px solid #000000;\"><p>1</p></td><td class=\"a1 a3\" style=\"width:103.7pt;border-top:0.5px solid #000000;border-bottom:0.5px solid #000000;border-left:0.5px solid #000000;border-right:0.5px solid #000000;\"><p>2</p></td><td class=\"a1 a3\" style=\"width:103.7pt;border-top:0.5px solid #000000;border-bottom:0.5px solid #000000;border-left:0.5px solid #000000;border-right:0.5px solid #000000;\"><p>1122</p></td><td class=\"a1 a3\" style=\"width:103.7pt;border-top:0.5px solid #000000;border-bottom:0.5px solid #000000;border-left:0.5px solid #000000;border-right:0.5px solid #000000;\"><p/></td></tr><tr class=\"a1 a3\"><td class=\"a1 a3\" style=\"width:103.7pt;border-top:0.5px solid #000000;border-bottom:0.5px solid #000000;border-left:0.5px solid #000000;border-right:0.5px solid #000000;\"><p>2</p></td><td class=\"a1 a3\" style=\"width:103.7pt;border-top:0.5px solid #000000;border-bottom:0.5px solid #000000;border-left:0.5px solid #000000;border-right:0.5px solid #000000;\"><p>2</p></td><td class=\"a1 a3\" style=\"width:103.7pt;border-top:0.5px solid #000000;border-bottom:0.5px solid #000000;border-left:0.5px solid #000000;border-right:0.5px solid #000000;\"><p/></td><td class=\"a1 a3\" style=\"width:103.7pt;border-top:0.5px solid #000000;border-bottom:0.5px solid #000000;border-left:0.5px solid #000000;border-right:0.5px solid #000000;\"><p/></td></tr></table><p/></div></body></html>";
//
//        String imagedir = "D://桌面/wordImage/";
//        htmlToDocx(contextHtml, new FileOutputStream("D://桌面/222.docx"), imagedir);
//
//    }
//
//    public static String replaceImage(String htmlContext, String imageReadUrl) {
//        if (!imageReadUrl.endsWith("/")) {
//            imageReadUrl = imageReadUrl + "/";
//        }
//        String s = htmlContext.replaceAll("\\\\", "/");
//        Document doc = Jsoup.parse(s);
//        for (Element img : doc.select("img")) {
//            String srcAfter = img.attributes().get("src");
//            String string = img.toString().replaceAll(">", "/>");
//            s = s.replaceAll(string, string.replaceAll(srcAfter, imageReadUrl + srcAfter));
//        }
//        return s;
//    }
//}
//
//
//
