package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.utils.operationUtils;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

@Getter
@AllArgsConstructor
public enum WordValueEnum {

    breakStr("\n"),//换行符号 在word 中是 回车

    UnderLineStart("\\[word_underline\\]"), //下划线开始符号
    UnderLineEnd("\\[/word_underline\\]"), //下划线结束符号

    boldStart("\\[word_bold\\]"),//加粗开始符号
    boldEnd("\\[/word_bold\\]") //加粗结束符号


    ;
    private String value;

    public String addContent(Object content, String defaultValue) {
        Object str = Objects.isNull(content) ? defaultValue : content;
        switch (this) {
            case breakStr:
                return str + breakStr.getValue();
            case UnderLineStart:
                return UnderLineStart.getValue() + str + UnderLineEnd.getValue();
            case boldStart:
            case boldEnd:
                return boldStart.getValue() + str + boldEnd.getValue();
            default:
                return str.toString();
        }
    }

    public String addContent(Object content) {
        return addContent(content, "    "); //默认两个空格
    }
}
