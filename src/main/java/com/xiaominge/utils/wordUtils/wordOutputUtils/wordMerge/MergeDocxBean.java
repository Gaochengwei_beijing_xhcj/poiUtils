package com.xiaominge.utils.wordUtils.wordOutputUtils.wordMerge;

import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.FontBean;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.FootBeans.FootPageBean;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.ParameterBean;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: system_platform
 * @description:
 * @author: xiaominge
 * @create: 2021-01-08 14:57
 **/

@Getter
public class MergeDocxBean {

    private File firstDocxFile;

    private List<nextDocxBean> nextDocxBeanList = new ArrayList<>();


    private String headerContext;
    private FontBean headerFontBean;
    private ParameterBean HeaderContent;
    //是否添加页脚
    private boolean isAddFoot;
    private FootPageBean footPageBean;
    private FontBean footPageFontBean;
    private List<ParameterBean> FootContentList;


    public MergeDocxBean(File firstDocxFile, List<nextDocxBean> nextDocxBeanList, String headerContext, FontBean headerFontBean, ParameterBean headerContent, FootPageBean footPageBean, FontBean footPageFontBean, List<ParameterBean> footContentList) {
        this.firstDocxFile = firstDocxFile;
        this.nextDocxBeanList = nextDocxBeanList;
        this.headerContext = headerContext;
        this.headerFontBean = headerFontBean;
        this.HeaderContent = headerContent;

        this.footPageBean = footPageBean;
        this.footPageFontBean = footPageFontBean;
        this.FootContentList = footContentList;
        if ((footContentList != null && !footContentList.isEmpty()) || footPageBean != null) {
            this.isAddFoot = true;
        }
    }

    public MergeDocxBean(File firstDocxFile, String headerContext, FontBean headerFontBean, FootPageBean footPageBean, FontBean footPageFontBean) {
        //设置页眉 就要删除页眉
        if (StringUtils.isNotBlank(headerContext)) {
            this.headerContext = headerContext;
            this.headerFontBean = headerFontBean;
        }

        if (footPageBean != null) {
            this.isAddFoot = true;
            this.footPageBean = footPageBean;
            if (footPageFontBean == null) {
                this.footPageFontBean = new FontBean("黑体", 9);
            } else {
                this.footPageFontBean = footPageFontBean;
            }

        }
        this.firstDocxFile = firstDocxFile;
    }

    public MergeDocxBean(File firstDocxFile, String headerContext, FontBean headerFontBean) {
        this.firstDocxFile = firstDocxFile;
        if (StringUtils.isNotBlank(headerContext)) {
            this.headerContext = headerContext;
            if (headerFontBean == null) {
                this.headerFontBean = new FontBean("黑体", 9);
            } else {
                this.headerFontBean = headerFontBean;
            }
        }

    }

    public MergeDocxBean(File firstDocxFile, FootPageBean footPageBean) {
        if (footPageBean != null) {
            this.isAddFoot = true;
        }
        this.firstDocxFile = firstDocxFile;
        this.footPageBean = footPageBean;
    }


    public MergeDocxBean(File firstDocxFile) {
        this.firstDocxFile = firstDocxFile;
    }

    public void addNext(File next, boolean isPageAppend) {
        nextDocxBean nextDocxBean = new nextDocxBean(next, isPageAppend);
        this.nextDocxBeanList.add(nextDocxBean);
    }


    @Getter
    @AllArgsConstructor
    class nextDocxBean {
        private File nextDocxFile;
        private boolean isPageAppend;
    }


}
