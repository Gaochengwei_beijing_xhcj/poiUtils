package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.textBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: data_centre_java
 * @description: 一个文本循环的for
 * @author: Administrator
 * @create: 2020-07-22 15:47
 **/
public class OneFor {
    //里面一个对象   就是循环的一个段落需要渲染的文字信息
    private List<Text> texts;

    //构造
    public OneFor() {
        this.texts = new ArrayList<>();
    }

    public Text createOneForText() {
        Text text = new Text();
        this.texts.add(text);
        return text;
    }

    public List<Text> getOneForTextList() {
        return texts;
    }

}