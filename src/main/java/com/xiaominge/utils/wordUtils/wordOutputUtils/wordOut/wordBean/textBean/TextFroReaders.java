package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.textBean;

import com.xiaominge.exception.ParameterRuntimeException;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: data_centre_java
 * @description: 个循环段落渲染的段落
 * @author: Administrator
 * @create: 2020-07-22 10:15
 **/

public class TextFroReaders {

    //里面一个对象就是一个循环
    private Map<String, OneFor> textForReader;


    //构造
    public TextFroReaders() {
        this.textForReader = new HashMap<>();
    }


    /**
     * @param forEachKey 创建 一个循环段落
     *                   UserSing 后面的变量
     *                   在模板中 以forEachKey_ 标志着需要改key 的识别
     *                   <p>
     *                   比如 在模板中 变量是: forEachKey_UserSing
     *                   在代码中 只需要写 后面的key  UserSing
     * @return
     */
    public OneFor CreateOneForText(String forEachKey) {
        if (forEachKey==null){
             ParameterRuntimeException.throwException("循环的key 标志不能为空");
        }
        OneFor onetextFor = new OneFor();
        //将当前的实例放入 数据
        this.textForReader.put(forEachKey, onetextFor);
        return onetextFor;
    }

    public OneFor getOneForTextList(String forKey) {
        return textForReader.get(forKey);
    }

}

