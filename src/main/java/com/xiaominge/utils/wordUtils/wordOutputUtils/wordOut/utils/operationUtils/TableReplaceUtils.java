package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.utils.operationUtils;

import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.ParameterBean;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.tableBean.OneCell;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: data_centre_java
 * @description: 替换变量工具
 * @author: Administrator
 * @create: 2020-07-23 10:12
 **/

@Slf4j
public class TableReplaceUtils {


    /**
     * 根据map替换段落元素内的{**}标签
     *
     * @param xWPFParagraph
     * @param OneCellList   替换的数据源
     * @author Juveniless
     * @date 2017年12月4日 下午3:09:00
     */
    public static  void replaceParagraph(XWPFParagraph xWPFParagraph,
                                 List<OneCell> OneCellList) {

        Map<String, ParameterBean> parametersMap = new HashMap<>();
        OneCellList.stream().forEach(OneCell -> parametersMap.put(OneCell.getKey(), OneCell));

        RunReplaceUtils.replaceParagraph(xWPFParagraph, parametersMap);

    }


    /**
     * 复制文本节点run
     *
     * @param newRun      新创建的的文本节点
     * @param templateRun 模板文本节点
     * @author Juveniless
     * @date 2017年11月27日 下午3:47:17
     */
    @SneakyThrows
    public static void copyRun(XWPFRun newRun, XWPFRun templateRun) {
        RunReplaceUtils.copyRun(newRun, templateRun);
    }
}
