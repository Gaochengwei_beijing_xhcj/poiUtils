package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.textBean;

import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.ParameterBean;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: data_centre_java
 * @description: 直接渲染的文本
 * @author: Administrator
 * @create: 2020-07-22 10:15
 **/

public class Text {

    private List<ParameterBean> texts;

    //构造
    public Text() {
        this.texts = new ArrayList<>();
    }

    public List<ParameterBean> getTexts() {
        return this.texts;
    }


    public void clear() {
        if (this.texts != null) {
            texts.clear();
        }
    }

    public void putValue(List<ParameterBean> texts) {
        if (texts == null) {
            throw new NullPointerException("渲染的参数不能为空");
        }

        if (this.texts == null) {
            this.texts = texts;
        } else {
            this.texts.addAll(texts);
        }

    }

    public void putValue(ParameterBean parameterBean) {
        this.texts.add(parameterBean);
    }

    public Text putValue(String key, Object value) {
        this.texts.add(new ParameterBean(key, value));
        return this;
    }
//添加默认值选项
    public Text putValue(String key, Object value, String defaultValue) {
        if (value != null) {
            this.texts.add(new ParameterBean(key, value));
        } else {
            this.texts.add(new ParameterBean(key, defaultValue));
        }

        return this;
    }

    public Text putValue(String key, Object value, boolean nextIsNewPage) {
        this.texts.add(new ParameterBean(key, value, nextIsNewPage));
        return this;
    }

    public Text putImage(String key, String imageType,
                         InputStream imageInputStream, int width, int height) {
        this.texts.add(new ParameterBean(key, imageType, imageInputStream, width, height));

        return this;
    }

    public Text putImage(String key, String imageType,
                         InputStream imageInputStream, int width, int height, boolean nextIsNewPage) {
        this.texts.add(new ParameterBean(key, imageType, imageInputStream, width, height, 0, 0, nextIsNewPage));

        return this;
    }

    public Text putImage(String key, String imageType,
                         InputStream imageInputStream, int width, int height, int leftOffset, int topOffset, boolean nextIsNewPage) {
        this.texts.add(new ParameterBean(key, imageType, imageInputStream, width, height, leftOffset, topOffset, nextIsNewPage));
        return this;
    }
}
