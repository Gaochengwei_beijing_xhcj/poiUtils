package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.tableBean;

import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.ParameterBean;
import lombok.Getter;
import lombok.Setter;

import java.io.InputStream;

/**
 * @program: data_centre_java
 * @description: 表格中一个单元格信息
 * @author: Administrator
 * @create: 2020-07-22 11:45
 **/
@Getter
@Setter
public class OneCell extends ParameterBean {


    private CellMergeBean cellMergeBean;

    public OneCell(String key, Object value) {
        super(key, value);
    }

    public OneCell(String key, String imageType, InputStream imageInputStream, int width, int height) {
        super(key, imageType, imageInputStream, width, height);
    }

    public OneCell(String key, String imageType, InputStream imageInputStream, int width, int height, int leftOffset, int topOffset) {
        super(key, imageType, imageInputStream, width, height, leftOffset, topOffset, false);
    }


    /**
     * 合并行 和列
     *
     * @param mergeNum 序号合并的行数  0-表示不需要合并行
     * @param startCol 开始列数
     * @param endCol   结束列数
     */
    public void createCellMergeBean(int mergeNum, int startCol, int endCol) {
        this.cellMergeBean = CellMergeBean.CreateRowAndColMergeBean(mergeNum, startCol, endCol);
    }

    /**
     * 指定行 合并列
     *
     * @param startCol 开始列 index
     * @param endCol   结束 index
     */
    public void createCellMergeBean(int startCol, int endCol) {
        this.cellMergeBean = CellMergeBean.CreateColMergeBean(startCol, endCol);

    }

    /**
     * 指定列 向下合并行
     *
     * @param mergeNum 向下合并多少行
     * @param startCol 哪一列需要合并
     */
    public void createRowMergeBean(int mergeNum, int startCol) {
        this.cellMergeBean = CellMergeBean.CreateRowMergeBean(mergeNum, startCol);
    }

}
