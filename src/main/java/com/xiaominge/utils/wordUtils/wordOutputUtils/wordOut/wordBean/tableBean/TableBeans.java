package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.tableBean;

import com.xiaominge.exception.ParameterRuntimeException;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: data_centre_java
 * @description: 表格信息
 * @author: Administrator
 * @create: 2020-07-22 11:44
 **/

public class TableBeans {
    //一个word 里面所有的表格
    Map<String, OneTable> tables;

    //构造
    public TableBeans() {
        this.tables = new HashMap<>();
    }


    /**
     *
     * @param oneTableKey 表格的key
     * @param tableType 表格的替换值类型
     * @return  获取一个表格对象
     */
    public OneTable CreateOneTable(String oneTableKey, TableType tableType) {
        if (StringUtils.isBlank(oneTableKey)) {
            throw new NullPointerException("表格key 不能为空");
        }
        if (tables.containsKey(oneTableKey)) {
            throw ParameterRuntimeException.throwException("表格的key 已经存在");
        }
        OneTable oneTable = new OneTable(oneTableKey, tableType);
        this.tables.put(oneTableKey, oneTable);
        return oneTable;
    }

    //放入一个表格
    public void addOneTable(OneTable oneTable) {
        if (StringUtils.isBlank(oneTable.getTableKey())) {
            throw new NullPointerException("表格key 不能为空");
        }
        if (tables.containsKey(oneTable.getTableKey())) {
            throw ParameterRuntimeException.throwException("表格的key 已经存在");
        }
        tables.put(oneTable.getTableKey(), oneTable);
    }

    public boolean containsKey(String tableKey) {
        return tables.containsKey(tableKey);
    }

    public OneTable getOneTable(String tableKey) {
        return tables.get(tableKey);
    }
}


