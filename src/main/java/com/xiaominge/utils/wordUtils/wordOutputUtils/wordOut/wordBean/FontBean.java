package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @program: system_platform
 * @description: 字体bean
 * @author: xiaominge
 * @create: 2021-01-04 09:33
 **/

@Getter
@AllArgsConstructor
public class FontBean {

    private String fontName;
    private int fontSize;
}

