package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.utils;

import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.utils.operationUtils.TableReplaceUtils;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.tableBean.CellMergeBean;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.tableBean.OneCell;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.tableBean.OneRow;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.tableBean.OneTable;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblGrid;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STMerge;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @program: data_centre_java
 * @description: word 表格处理
 * @author: Administrator
 * @create: 2020-07-22 18:58
 **/

public class WordTableUtils {


    private XWPFDocument document;

    public WordTableUtils(XWPFDocument document) {
        this.document = document;
    }


    /**
     * @param templateTable 操作的表格
     * @param oneTable      一个表格
     * @param flag          (0为静态表格，1为表格整体循环，2为表格内部行循环，3为表格不循环仅简单替换标签即可)
     */
    public void addTableInDocFooter(XWPFTable templateTable,
                                    OneTable oneTable, int flag) {

        if (flag == 1) {// 表格整体循环
            for (OneRow row : oneTable.getRows()) {

                List<XWPFTableRow> templateTableRows = templateTable.getRows();// 获取模板表格所有行
                XWPFTable newCreateTable = createTable();// 创建新表格,默认一行一列
                for (int i = 1; i < templateTableRows.size(); i++) {
                    XWPFTableRow newCreateRow = newCreateTable.createRow(); //创建一行
                    CopyTableRow(newCreateRow, templateTableRows.get(i));// 复制模板行文本和样式到新行 包含列
                }
                newCreateTable.removeRow(0);// 移除多出来的第一行
                document.createParagraph();// 添加回车换行
                replaceTable(newCreateTable, row.getCells());//替换标签
            }

        } else if (flag == 2) { // 表格表格内部行循环
            XWPFTable newCreateTable = createTable();// 创建新表格, 默认一行一列
            List<XWPFTableRow> TempTableRows = templateTable.getRows();// 获取模板表格所有行
            int tagRowsIndex = 0;// 标签行indexs

            for (int i = 0, size = TempTableRows.size(); i < size; i++) {
                String rowText = TempTableRows.get(i).getCell(0).getText();// 获取到表格行的第一个单元格
                if (rowText.contains(WordTemplateKeyEnum.tableRowForEachStart.getKeyCode())) {
                    tagRowsIndex = i;
                    break;
                }
            }

            /* 复制模板行和标签行之前的行 类似于表格头部 */
            for (int i = 1; i < tagRowsIndex; i++) {
                XWPFTableRow newCreateRow = newCreateTable.createRow();
                CopyTableRow(newCreateRow, TempTableRows.get(i));// 复制行
                replaceTableRow(newCreateRow, oneTable.getTableTitleRow().getCells());// 处理不循环标签的替换
            }

            /* 循环生成模板行 */
            List<CellMergeBean> cellMergeBeans = new ArrayList<>();
            XWPFTableRow tempRow = TempTableRows.get(tagRowsIndex + 1);// 获取到模板行
            for (int i = 0; i < oneTable.getRows().size(); i++) {
                OneRow oneRow = oneTable.getRows().get(i);
                XWPFTableRow newCreateRow = newCreateTable.createRow();
                CopyTableRow(newCreateRow, tempRow);// 复制模板行
                replaceTableRow(newCreateRow, oneRow.getCells());// 处理标签替换

                //获取设置合并单元格的cell
                List<CellMergeBean> mergeCells = oneRow.getCells().stream().map(OneCell::getCellMergeBean).filter(Objects::nonNull).collect(Collectors.toList());
                for (CellMergeBean mergeCell : mergeCells) {

                    mergeCell.setStartRow(i + tagRowsIndex); //设置合并单元格开始的行
                    cellMergeBeans.add(mergeCell);
                }

            }
            //循环 处理合并行 只有循环里面才可设置合并单元格
            Map<Integer, Integer> colToMergeCount = new HashMap<>();
            for (CellMergeBean cellMergeBean : cellMergeBeans) {
                //合并指定行的列
                mergerCols(newCreateTable, cellMergeBean.getStartRow(), cellMergeBean.getStartCol(), cellMergeBean.getEndCol());

                //既要合并行  又要合并列  先循环行 合并列
                if (cellMergeBean.getMergeNum() > 0 && (cellMergeBean.getStartCol() != cellMergeBean.getEndCol())) {
                    for (int i = 0; i < cellMergeBean.getMergeNum(); i++) {
                        //合并列
                        mergerCols(newCreateTable, cellMergeBean.getStartRow() + i + 1, cellMergeBean.getStartCol(), cellMergeBean.getEndCol());
                    }
                }
                //合并行
                mergeRows(newCreateTable, cellMergeBean.getStartCol(), cellMergeBean.getStartRow(), cellMergeBean.getMergeNum() + cellMergeBean.getStartRow());
            }


            /* 复制模板行和标签行之后的行 */
            for (int i = tagRowsIndex + 2; i < TempTableRows.size(); i++) {
                XWPFTableRow newCreateRow = newCreateTable.createRow();
                CopyTableRow(newCreateRow, TempTableRows.get(i));// 复制行
                replaceTableRow(newCreateRow, oneTable.getTableEndRow().getCells()); // 处理不循环标签的替换  表格尾部的行 如果有信息可以替换
            }
            newCreateTable.removeRow(0);// 移除多出来的第一行
            document.createParagraph();// 添加回车换行


        } else if (flag == 3) {
            //表格不循环 仅简单替换标签
            List<XWPFTableRow> templateTableRows = templateTable.getRows();// 获取模板表格所有行
            XWPFTable newCreateTable = createTable();// 创建新表格,默认一行一列
            for (int i = 1; i < templateTableRows.size(); i++) { //从1 开始 第一行是标志 -->不要
                XWPFTableRow newCreateRow = newCreateTable.createRow();
                CopyTableRow(newCreateRow, templateTableRows.get(i));// 复制模板行文本和样式到新行
            }
            newCreateTable.removeRow(0);// 移除多出来的第一行
            document.createParagraph();// 添加回车换行

            List<OneCell> cells = new ArrayList<>();
            if (oneTable.getTableTitleRow().getCells() != null) {
                cells.addAll(oneTable.getTableTitleRow().getCells());
            }
            if (oneTable.getTableEndRow().getCells() != null) {
                cells.addAll(oneTable.getTableEndRow().getCells());
            }
            replaceTable(newCreateTable, cells); //只是替换的数据 只需要放入标题  或者放入 尾部集合

        } else if (flag == 0) {
            List<XWPFTableRow> templateTableRows = templateTable.getRows();// 获取模板表格所有行
            XWPFTable newCreateTable = createTable();// 创建新表格,默认一行一列
            for (int i = 1; i < templateTableRows.size(); i++) {  //从1 开始 第一行是标志 -->不要
                XWPFTableRow newCreateRow = newCreateTable.createRow();
                CopyTableRow(newCreateRow, templateTableRows.get(i));// 复制模板行文本和样式到新行
            }
            newCreateTable.removeRow(0);// 移除多出来的第一行
            document.createParagraph();// 添加回车换行
        }

    }


    /**
     * 复制表格行XWPFTableRow格式
     *
     * @param target 待修改格式的XWPFTableRow
     * @param source 模板XWPFTableRow
     */
    private void CopyTableRow(XWPFTableRow target, XWPFTableRow source) {

        int tempRowCellsize = source.getTableCells().size();// 模板行的列数
        for (int i = 0; i < tempRowCellsize - 1; i++) {
            target.addNewTableCell();// 为新添加的行添加与模板表格对应行行相同个数的单元格
        }
        // 复制样式
        target.getCtRow().setTrPr(source.getCtRow().getTrPr());
        // 复制单元格
        for (int i = 0; i < target.getTableCells().size(); i++) {
            copyTableCell(target.getCell(i), source.getCell(i));
        }
    }

    /**
     * 复制单元格XWPFTableCell格式
     *
     * @param newTableCell      新创建的的单元格
     * @param templateTableCell 模板单元格
     * @author Juveniless
     * @date 2017年11月27日 下午3:41:02
     */
    private void copyTableCell(XWPFTableCell newTableCell, XWPFTableCell templateTableCell) {
        // 列属性
        newTableCell.getCTTc().setTcPr(templateTableCell.getCTTc().getTcPr());
        // 删除目标 targetCell 所有文本段落
        for (int pos = 0; pos < newTableCell.getParagraphs().size(); pos++) {
            newTableCell.removeParagraph(pos);
        }
        // 添加新文本段落
        for (XWPFParagraph sp : templateTableCell.getParagraphs()) {
            XWPFParagraph targetP = newTableCell.addParagraph();
            copyParagraph(targetP, sp);
        }
    }

    /**
     * 复制文本段落XWPFParagraph格式
     *
     * @param newParagraph      新创建的的段落
     * @param templateParagraph 模板段落
     * @author Juveniless
     * @date 2017年11月27日 下午3:43:08
     */
    private void copyParagraph(XWPFParagraph newParagraph, XWPFParagraph templateParagraph) {
        // 设置段落样式
        newParagraph.getCTP().setPPr(templateParagraph.getCTP().getPPr());
        // 添加Run标签
        for (int pos = 0; pos < newParagraph.getRuns().size(); pos++) {
            newParagraph.removeRun(pos);

        }
        for (XWPFRun s : templateParagraph.getRuns()) {
            XWPFRun targetrun = newParagraph.createRun();
            TableReplaceUtils.copyRun(targetrun, s);
        }

    }

    /**
     * 根据参数parametersMap对表格的一行进行标签的替换
     *
     * @param tableRow    表格行
     * @param oneRowCells 表格的一行数据
     * @author Juveniless
     * @date 2017年11月23日 下午2:09:24
     */
    public void replaceTableRow(XWPFTableRow tableRow, List<OneCell> oneRowCells) {

        List<XWPFTableCell> tableCells = tableRow.getTableCells();
        for (XWPFTableCell xWPFTableCell : tableCells) {
            List<XWPFParagraph> paragraphs = xWPFTableCell.getParagraphs();
            for (XWPFParagraph xwpfParagraph : paragraphs) {
                TableReplaceUtils.replaceParagraph(xwpfParagraph, oneRowCells);
            }
        }

    }


    /**
     * 根据map替换表格中的{key}标签
     *
     * @param xwpfTable
     * @param cells     表格的参数
     * @author Juveniless
     * @date 2017年12月4日 下午2:47:36
     */
    public void replaceTable(XWPFTable xwpfTable, List<OneCell> cells) {
        List<XWPFTableRow> rows = xwpfTable.getRows();
        for (XWPFTableRow xWPFTableRow : rows) {
            List<XWPFTableCell> tableCells = xWPFTableRow.getTableCells();
            for (XWPFTableCell xWPFTableCell : tableCells) {
                List<XWPFParagraph> paragraphs2 = xWPFTableCell.getParagraphs();
                for (XWPFParagraph xWPFParagraph : paragraphs2) {
                    TableReplaceUtils.replaceParagraph(xWPFParagraph, cells);
                }
            }
        }

    }


    // word跨列合并单元格  合并列
    public void mergerCols(XWPFTable table, int row, int fromCell, int toCell) {

        for (int cellIndex = fromCell; cellIndex <= toCell; cellIndex++) {
            XWPFTableCell cell = table.getRow(row).getCell(cellIndex);
            if (cellIndex == fromCell) {
                // The first merged cell is set with RESTART merge value
                cell.getCTTc().addNewTcPr().addNewHMerge().setVal(STMerge.RESTART);
            } else {
                // Cells which join (merge) the first one, are set with CONTINUE
                cell.getCTTc().addNewTcPr().addNewHMerge().setVal(STMerge.CONTINUE);
            }
        }
    }

    // word跨行并单元格   合并行
    public void mergeRows(XWPFTable table, int col, int fromRow, int toRow) {
        for (int rowIndex = fromRow; rowIndex <= toRow; rowIndex++) {
            XWPFTableCell cell = table.getRow(rowIndex).getCell(col);
            if (rowIndex == fromRow) {
                // The first merged cell is set with RESTART merge value
                cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.RESTART);
            } else {
                // Cells which join (merge) the first one, are set with CONTINUE
                cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.CONTINUE);
            }
        }
    }

    /**
     * 创建表格
     */
    public XWPFTable createTable() {
        XWPFTable table = document.createTable();
        // 校验一下grid是否为空，如果为空就创建。转pdf的时候如果为空会报空指针
        CTTblGrid grid = table.getCTTbl().getTblGrid();
        if (grid == null) {
            table.getCTTbl().addNewTblGrid();
        }
        return table;
    }
}
