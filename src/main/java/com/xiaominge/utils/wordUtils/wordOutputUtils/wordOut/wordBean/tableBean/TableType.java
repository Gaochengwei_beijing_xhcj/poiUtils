package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.tableBean;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TableType {
    for_Row,                 //("forRow","表格行循环"),
    for_table,                 //("forTable","表格整体循环"),
    replace_value,                 //("replaceValue","替换值,值放在标题或者 尾部"),
    No_todo                 //("noTodo","简单复制一个表格 =不做任何事情");


    ;


}
