package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.headerUtils;

import com.xiaominge.utils.IdUtils;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.FontBean;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.ParameterBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;

import java.math.BigInteger;

/**
 * @program: system_platform
 * @description: 创建页眉
 * @author: xiaominge
 * @create: 2021-01-04 09:12
 **/

public class CreateHeaderUtils {


    /**
     * @param doc
     * @param heardContext
     * @throws Exception
     */
    public static void createHeader(XWPFDocument doc, FontBean fontBean, String heardContext, ParameterBean imageParameterBean) throws Exception {
        /*
         * 对页眉段落作处理，使logo图片在页眉左边，公司全称在页眉右边
         * */
        CTSectPr sectPr = doc.getDocument().getBody().addNewSectPr();
        XWPFHeaderFooterPolicy headerFooterPolicy = new XWPFHeaderFooterPolicy(doc, sectPr);
        XWPFHeader header = headerFooterPolicy.createHeader(XWPFHeaderFooterPolicy.DEFAULT);

        XWPFParagraph paragraph = header.getParagraphArray(0);
        if(paragraph==null){
            paragraph= header.createParagraph();
        }
        paragraph.setAlignment(ParagraphAlignment.LEFT);
        paragraph.setBorderBottom(Borders.THICK);

        CTTabStop tabStop = paragraph.getCTP().getPPr().addNewTabs().addNewTab();
        tabStop.setVal(STTabJc.RIGHT);
        int twipsPerInch = 1440;
        tabStop.setPos(BigInteger.valueOf(6 * twipsPerInch));

        XWPFRun run = paragraph.createRun();
        setXWPFRunStyle(run, fontBean);


        //写入图片到页眉
        if (imageParameterBean != null && imageParameterBean.getImageInputStream() != null) {

            XWPFPicture picture = run.addPicture(imageParameterBean.getImageInputStream(),
                    imageParameterBean.getImageTypeCode(),
                    imageParameterBean.getValue() == null ? IdUtils.getId() : imageParameterBean.getValue().toString(),
                    Units.toEMU(imageParameterBean.getWidth()),
                    Units.toEMU(imageParameterBean.getHeight())
            );

            String blipID = "";
            for (XWPFPictureData picturedata : header.getAllPackagePictures()) {    //这段必须有，不然打开的图片不显示
                blipID = header.getRelationId(picturedata);
            }
            picture.getCTPicture().getBlipFill().getBlip().setEmbed(blipID);
            run.addBreak();
            imageParameterBean.getImageInputStream().close();
        }
        /*
         * 添加字体页眉，公司全称
         * 公司全称在右边
         * */
        if (StringUtils.isNotBlank(heardContext)) {
            run = paragraph.createRun();
            run.setText(heardContext);
            setXWPFRunStyle(run, fontBean);
        }
    }

    /**
     * 设置字体样式
     *
     * @param xwpfRun 段落元素
     */
    public static void setXWPFRunStyle(XWPFRun xwpfRun, FontBean fontBean) {
        xwpfRun.setFontSize(fontBean.getFontSize());
        CTRPr rpr = xwpfRun.getCTR().isSetRPr() ? xwpfRun.getCTR().getRPr() : xwpfRun.getCTR().addNewRPr();
        CTFonts fonts = rpr.sizeOfRFontsArray()!=0 ? rpr.getRFontsArray(rpr.getRFontsArray().length-1) : rpr.addNewRFonts();
        fonts.setAscii(fontBean.getFontName());
        fonts.setEastAsia(fontBean.getFontName());
        fonts.setHAnsi(fontBean.getFontName());
    }

}
