package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.utils.operationUtils;

import com.xiaominge.utils.IdUtils;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.utils.WordTemplateKeyEnum;
import com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.wordBean.ParameterBean;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGraphicalObject;
import org.openxmlformats.schemas.drawingml.x2006.wordprocessingDrawing.CTAnchor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDrawing;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRPr;
import org.springframework.beans.BeanUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @program: data_centre_java
 * @description: 替换变量工具
 * @author: Administrator
 * @create: 2020-07-23 10:12
 **/

@Slf4j
public class RunReplaceUtils {

    private XWPFDocument document;

    public RunReplaceUtils(XWPFDocument xwpfDocument) {
        this.document = xwpfDocument;
    }

    /**
     * 根据map替换段落元素内的{**}标签
     *
     * @param xWPFParagraph
     * @param parameterBeanList 替换的数据源
     * @author Juveniless
     * @date 2017年12月4日 下午3:09:00
     */

    public static void replaceParagraph(XWPFParagraph xWPFParagraph,
                                        List<ParameterBean> parameterBeanList) {
        Map<String, ParameterBean> parametersMap = new HashMap<>();
        parameterBeanList.stream().forEach(OneCell -> parametersMap.put(OneCell.getKey(), OneCell));
        replaceParagraph(xWPFParagraph, parametersMap);
    }

    public static void replaceParagraph(XWPFParagraph xWPFParagraph,
                                        Map<String, ParameterBean> parametersMap) {


        List<XWPFRun> runs = xWPFParagraph.getRuns();
        String xWPFParagraphText = xWPFParagraph.getParagraphText();
        String regEx = "\\{.+?\\}";
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(xWPFParagraphText);//正则匹配字符串{****}

        if (matcher.find()) {
            // 查找到有标签才执行替换
            int beginRunIndex = xWPFParagraph.searchText(WordTemplateKeyEnum.keyStart.getKeyCode(), new PositionInParagraph()).getBeginRun();// 标签开始run位置
            int endRunIndex = xWPFParagraph.searchText(WordTemplateKeyEnum.keyEnd.getKeyCode(), new PositionInParagraph()).getEndRun();// 结束标签
            StringBuffer key = new StringBuffer();

            if (beginRunIndex == endRunIndex) {
                // {**}在一个run标签内
                XWPFRun beginRun = runs.get(beginRunIndex);
                XmlObject beginRunCtrPr = null;
                if (beginRun != null && beginRun.getCTR() != null && beginRun.getCTR().getRPr() != null) {
                    beginRunCtrPr = beginRun.getCTR().getRPr().copy();
                }
                String beginRunText = beginRun.toString();

                int beginIndex = beginRunText.indexOf(WordTemplateKeyEnum.keyStart.getKeyCode());
                int endIndex = beginRunText.indexOf(WordTemplateKeyEnum.keyEnd.getKeyCode());
                int length = beginRunText.length();

                if (beginIndex == 0 && endIndex == length - 1) {
                    //获取替换变量 key
                    key.append(beginRunText, 1, endIndex);
                    String keyString = key.toString();
                    ParameterBean wordValueBeanByKey = getWordValueBeanByKey(keyString, parametersMap);
                    // 该run标签只有{**}
                    XWPFRun insertNewRun = newRunReplace(xWPFParagraph, beginRunIndex, beginRunCtrPr);

                    //判断是否强制分页
                    if (wordValueBeanByKey != null) {
                        if (wordValueBeanByKey.isNextIsNewPage()) {
                            insertNewRun.addBreak(BreakType.PAGE); //强制分页
                        }
                    }

                    //根据key 类型 替换变量 图片还是文字
                    if (keyString.startsWith(WordTemplateKeyEnum.imageKeyPerfix.getKeyCode())) { //如果是以image_开头的 渲染图片
                        addPicture(insertNewRun, wordValueBeanByKey, null, null);
                    } else { //文本
                        int addCount = setValue(wordValueBeanByKey, insertNewRun, beginRunIndex, xWPFParagraph, beginRunCtrPr, "");
                        endRunIndex += addCount;

                    }

                } else {
                    //获取替换变量 key
                    key.append(beginRunText, beginRunText.indexOf(WordTemplateKeyEnum.keyStart.getKeyCode()) + 1, beginRunText.indexOf(WordTemplateKeyEnum.keyEnd.getKeyCode()));
                    String keyString = key.toString();
                    ParameterBean wordValueBeanByKey = getWordValueBeanByKey(keyString, parametersMap);

                    // 该run标签为**{**}** 或者 **{**} 或者{**}**，替换key后，还需要加上原始key前后的文本

                    XWPFRun insertNewRun = newRunReplace(xWPFParagraph, beginRunIndex, beginRunCtrPr);
                    //判断是否强制分页
                    if (wordValueBeanByKey != null) {
                        if (wordValueBeanByKey.isNextIsNewPage()) {
                            insertNewRun.addBreak(BreakType.PAGE); //强制分页
                        }
                    }
                    if (keyString.startsWith(WordTemplateKeyEnum.imageKeyPerfix.getKeyCode())) {  //如果是以image_开头的 渲染图片
                        addPicture(insertNewRun, wordValueBeanByKey, beginRunText.substring(0, beginIndex), beginRunText.substring(endIndex + 1));
                    } else {
                        //设置原本变量之前的内容
                        String before = beginRunText.substring(0, beginIndex);
                        setValue(insertNewRun, before);

                        //设置值 重新创建一个
                        int newRunIndex = beginRunIndex + 1;
                        int addCount = setValue(wordValueBeanByKey, newRunReplace(xWPFParagraph, newRunIndex, beginRunCtrPr), newRunIndex, xWPFParagraph, beginRunCtrPr, "");

                        //设置原本变量之后的内容 创建一个新的run 进行赋值
                        String after = beginRunText.substring(endIndex + 1);
                        setValue(newRunReplace(xWPFParagraph, newRunIndex + addCount + 1, beginRunCtrPr), after);

                    }

                }

            } else {
                // {**}被分成多个run
                //先处理起始run标签,取得第一个{key}值
                XWPFRun beginRun = runs.get(beginRunIndex);
                XmlObject beginRunCtrPr = null;
                if (beginRun != null && beginRun.getCTR() != null && beginRun.getCTR().getRPr() != null) {
                    beginRunCtrPr = beginRun.getCTR().getRPr().copy();
                }
                String beginRunText = beginRun.toString();
                int beginIndex = beginRunText.indexOf(WordTemplateKeyEnum.keyStart.getKeyCode());
                if (beginRunText.length() > 1) {
                    key.append(beginRunText.substring(beginIndex + 1));
                }
                // ArrayList<Integer> removeRunList = new ArrayList<>();//需要移除的run
                //处理中间的run
                for (int i = beginRunIndex + 1; i < endRunIndex; i++) {
                    XWPFRun run = runs.get(i);
                    XmlObject runCtrPr = null;
                    if (run != null && run.getCTR() != null && run.getCTR().getRPr() != null) {
                        runCtrPr = run.getCTR().getRPr().copy();
                    }
                    String runText = run.toString();
                    key.append(runText);
                    XWPFRun xwpfRun = newRunReplace(xWPFParagraph, i, runCtrPr);
                    xwpfRun.setText("");
                    // removeRunList.add(i);
                }

                // 获取endRun中的key值
                XWPFRun endRun = runs.get(endRunIndex);
                XmlObject endRunCTRPr = null;
                if (endRun != null && endRun.getCTR() != null && endRun.getCTR().getRPr() != null) {
                    endRunCTRPr = endRun.getCTR().getRPr().copy();
                }

                String endRunText = endRun.toString();
                int endIndex = endRunText.indexOf(WordTemplateKeyEnum.keyEnd.getKeyCode());
                //run中**}或者**}**
                if (endRunText.length() > 1 && endIndex != 0) {
                    key.append(endRunText.substring(0, endIndex));
                }
                //*******************************************************************
                //取得key值后替换标签

                //先处理开始标签
                if (beginRunText.length() == 1) { //该run 只有{
                    String keyString = key.toString();
                    ParameterBean wordValueBeanByKey = getWordValueBeanByKey(keyString, parametersMap);
                    // run标签内文本{
                    XWPFRun insertNewRun = newRunReplace(xWPFParagraph, beginRunIndex, beginRunCtrPr);
                    //判断是否强制分页
                    if (wordValueBeanByKey != null) {
                        if (wordValueBeanByKey.isNextIsNewPage()) {
                            insertNewRun.addBreak(BreakType.PAGE); //强制分页
                        }
                    }
                    if (keyString.startsWith(WordTemplateKeyEnum.imageKeyPerfix.getKeyCode())) {  //如果是以image_开头的 渲染图片
                        addPicture(insertNewRun, wordValueBeanByKey, null, null);
                    } else {
                        //带换行符的赋值
                        int addCount = setValue(wordValueBeanByKey, insertNewRun, beginRunIndex, xWPFParagraph, beginRunCtrPr, "");
                        endRunIndex += addCount;
                    }

                } else {

                    String keyString = key.toString();
                    ParameterBean wordValueBeanByKey = getWordValueBeanByKey(keyString, parametersMap);
                    // 该run标签为**{**或者 {** ，替换key后，还需要加上原始key前的文本
                    XWPFRun insertNewRun = newRunReplace(xWPFParagraph, beginRunIndex, beginRunCtrPr);//插入新run
                    //判断是否强制分页
                    if (wordValueBeanByKey != null) {
                        if (wordValueBeanByKey.isNextIsNewPage()) {
                            insertNewRun.addBreak(BreakType.PAGE); //强制分页
                        }
                    }
                    if (keyString.startsWith(WordTemplateKeyEnum.imageKeyPerfix.getKeyCode())) { //如果是以image_开头的 渲染图片
                        addPicture(insertNewRun, wordValueBeanByKey, null, null);
                    } else {
                        //带换行符的赋值
                        String before = beginRunText.substring(0, beginRunText.indexOf(WordTemplateKeyEnum.keyStart.getKeyCode()));
                        //带换行符的赋值
                        setValue(insertNewRun, before);
                        // 重新创建一个设置值
                        int newRunIndex = beginRunIndex + 1;
                        int addCount = setValue(wordValueBeanByKey, newRunReplace(xWPFParagraph, newRunIndex, beginRunCtrPr), newRunIndex, xWPFParagraph, beginRunCtrPr, "");
                        endRunIndex += addCount;

                    }

                }

                //处理结束标签
                if (endRunText.length() == 1) {
                    // run标签内文本只有}
                    XWPFRun insertNewRun = newRunReplace(xWPFParagraph, endRunIndex, endRunCTRPr);
                    // 设置文本
                    insertNewRun.setText("");
                } else {
                    // 该run标签为**}**或者 }** 或者**}，替换key后，还需要加上原始key后的文本
                    XWPFRun insertNewRun = newRunReplace(xWPFParagraph, endRunIndex, endRunCTRPr);
                    // 设置文本
                    String textString = endRunText.substring(endRunText.indexOf(WordTemplateKeyEnum.keyEnd.getKeyCode()) + 1);
                    // insertNewRun.setText(textString);
                    //带换行赋值
                    setValue(insertNewRun, textString);
                }

          /*      //处理中间的run标签
                for (int i = 0; i < removeRunList.size(); i++) {
                    XWPFRun xWPFRun = runs.get(removeRunList.get(i));//原始run
                    XWPFRun insertNewRun = getXwpfRun(xWPFParagraph, removeRunList.get(i), xWPFRun);
                    insertNewRun.setText("");
                    xWPFParagraph.removeRun(removeRunList.get(i) + 1);//移除原始的run
                }*/

            }// 处理${**}被分成多个run

            replaceParagraph(xWPFParagraph, parametersMap);

        }//if 有标签

    }

    private static void removeRun(XWPFParagraph xWPFParagraph, int beginRunIndex) {
        xWPFParagraph.removeRun(beginRunIndex);
    }

    private static XWPFRun newRunReplace(XWPFParagraph xWPFParagraph, int beginRunIndex, XmlObject rPr) {
        XWPFRun insertNewRun = insertNewRun(xWPFParagraph, beginRunIndex, rPr);
        removeRun(xWPFParagraph, beginRunIndex + 1);
        return insertNewRun;
    }

    private static XWPFRun insertNewRun(XWPFParagraph xWPFParagraph, int beginRunIndex, XmlObject rPr) {
        XWPFRun insertNewRun = xWPFParagraph.insertNewRun(beginRunIndex);
        CTR ctr = insertNewRun.getCTR();
        ctr.setRPr((CTRPr) rPr);
        return insertNewRun;
    }


    /**
     * 复制文本节点run
     *
     * @param newRun      新创建的的文本节点
     * @param templateRun 模板文本节点
     * @author Juveniless
     * @date 2017年11月27日 下午3:47:17
     */
    @SneakyThrows
    public static void copyRun(XWPFRun newRun, XWPFRun templateRun) {
        newRun.getCTR().setRPr(templateRun.getCTR().getRPr());
        // 设置文本
        newRun.setText(templateRun.toString());
        //复制图片
        List<XWPFPicture> embeddedPictures = templateRun.getEmbeddedPictures();
        if (embeddedPictures != null) {
            for (XWPFPicture embeddedPicture : embeddedPictures) {
                newRun.addPicture(embeddedPicture.getPictureData().getPackagePart().getInputStream(), embeddedPicture.getPictureData().getPictureType(),
                        embeddedPicture.getPictureData().getFileName(), Units.toEMU(embeddedPicture.getWidth()), Units.toEMU(embeddedPicture.getDepth()));
            }
        }
    }


    //给变量增加图片
    private static void addPicture(XWPFRun insertNewRun, ParameterBean parameterBean,
                                   String beforeStr, String afterStr) {
        InputStream imageInputStream = parameterBean.getImageInputStream();
        try {
            if (beforeStr != null) {
                setValue(insertNewRun, beforeStr);
            }
            insertNewRun.addPicture(
                    imageInputStream,
                    parameterBean.getImageTypeCode(),
                    parameterBean.getValue() == null ? IdUtils.getId() : parameterBean.getValue().toString(),
                    Units.toEMU(parameterBean.getWidth()),
                    Units.toEMU(parameterBean.getHeight())
            );
           /* // 2. 获取到图片数据
            CTDrawing drawing = insertNewRun.getCTR().getDrawingArray(0);
            CTGraphicalObject graphicalobject = drawing.getInlineArray(0).getGraphic();

            //拿到新插入的图片替换添加CTAnchor 设置浮动属性 删除inline属性
            CTAnchor anchor = getAnchorWithGraphic(graphicalobject, "TEST1",
                    Units.toEMU(parameterBean.getWidth()),
                    Units.toEMU(parameterBean.getHeight()),//图片大小
                    Units.toEMU(parameterBean.getLeftOffset()),  //左右偏移量
                    Units.toEMU(parameterBean.getTopOffset()),  //上线偏移量
                    false);
            drawing.setAnchorArray(new CTAnchor[]{anchor});//添加浮动属性
            drawing.removeInline(0);//删除行内属性*/

            if (afterStr != null) {
                setValue(insertNewRun, afterStr);
            }
        } catch (InvalidFormatException e) {
            e.printStackTrace();
            throw new RuntimeException("图片格式错误" + e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("图片文件写入错误" + e);
        } finally {
            try {
                imageInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //根据key 获取value对象
    private static ParameterBean getWordValueBeanByKey(String key, Map<String, ParameterBean> map) {
        ParameterBean parameterBean = null;
        if (key != null) {
            if (map.containsKey(key)) {
                parameterBean = map.get(key);
            } else {
                // todo 根据自己项目自己定义 如果模版里面设置了变了 导出的数据中没有 可以抛出异常 也可以默认替换为空格
                throw new RuntimeException("数据源中没有发现key=[" + key + "],的数据"); //抛出异常
                // returnValue = "";
//                return  new ParameterBean(key, ""); //默认替换为空格
            }
            Object value = parameterBean.getValue();
            if (value != null) {
                if (value instanceof String) {
                    parameterBean.setValue(StringUtils.remove(StringUtils.remove((String) value, WordTemplateKeyEnum.keyStart.getKeyCode()), WordTemplateKeyEnum.keyEnd.getKeyCode()));
                } else if (value.toString().contains(WordTemplateKeyEnum.keyStart.getKeyCode()) || value.toString().contains(WordTemplateKeyEnum.keyEnd.getKeyCode())) {
                    parameterBean.setValue(StringUtils.remove(StringUtils.remove(value.toString(), WordTemplateKeyEnum.keyStart.getKeyCode()), WordTemplateKeyEnum.keyEnd.getKeyCode()));
                }
            }
        }
        return parameterBean;
    }

/*    //根据key 获取value的值
    private String getValueByKey(String key, Map<String, ParameterBean> map) {
        String returnValue = "";
        if (key != null) {
            try {
                returnValue = map.get(key) != null ? map.get(key).getValue().toString() : "";
            } catch (Exception e) {
                // TODO: handle exception
                log.info("key:" + key + "***" + e);
                returnValue = "";
            }

        }
        return returnValue;
    }*/

    public static void setValue(XWPFRun run, String value) {
        run.setText(value);
    }

    //给变量赋值
    private static int setValue(ParameterBean parameterBean, XWPFRun insertNewRun, int insertNewRunIndex, XWPFParagraph xwpfParagraph, XmlObject rPr, String defaultValue) {
        //如果传入的值 是空的 就设置默认值
        if (parameterBean == null || parameterBean.getValue() == null) {
            insertNewRun.setText(defaultValue);
            return 0;
        }
        if (!(parameterBean.getValue() instanceof String)) { //如果value 不是string 类型 直接写入toString方法的返回值ddd
            insertNewRun.setText(parameterBean.getValue().toString());
            return 0;
        }
        //下换线寻找切割
        AtomicBoolean isSet = new AtomicBoolean(false);
        List<ParameterBean> parameterBeans_underLine = parseValue(parameterBean, WordValueEnum.UnderLineStart, WordValueEnum.UnderLineEnd, (old, New) -> {
            New.setUnderlinePatterns(old.getUnderlinePatternsValueUnderLineHtml());
        }, (old, New) -> {

        });

        int addCount = 0;
        //加粗寻找切割
        for (ParameterBean bean : parameterBeans_underLine) {
            //下换线寻找切割
            List<ParameterBean> parameterBeansBold = parseValue(bean, WordValueEnum.boldStart, WordValueEnum.boldEnd, (old, New) -> {
                if (old.getUnderlinePatterns() != null) {
                    New.setUnderlinePatterns(old.getUnderlinePatterns());
                    New.setUnderlineColor(old.getUnderlineColor());
                }
                //之前没有设置加粗 就要设置加粗
                Boolean bold = old.getBold();
                if (bold == null) {
                    bold = true;
                    isSet.set(true);
                }
                New.setBold(bold);
            }, (old, New) -> { //标签之外的 加粗要去除 如果触发了设置就要一处影响
                if (isSet.get()) {
                    New.setBold(false);
                }
            });

            for (ParameterBean doSetValueBean : parameterBeansBold) {
                XWPFRun newRun;
                if (addCount == 0) {
                    newRun = insertNewRun;
                } else {
                    newRun = insertNewRun(xwpfParagraph, insertNewRunIndex + addCount, rPr);

                }

                String valueTemp = StringEscapeUtils.unescapeJava((String) doSetValueBean.getValue());
                //如果代码里面设置了加粗 就设置加粗
                if (doSetValueBean.getBold() != null) {
                    newRun.setBold(doSetValueBean.getBold());
                }
                //如果代码里面设置了下划线 就设置下划线
                if (doSetValueBean.getUnderlinePatterns() != null) {
                    newRun.setUnderline(doSetValueBean.getUnderlinePatterns());
                    newRun.setUnderlineColor(doSetValueBean.getUnderlineColor()); //设置下划线颜色
                }

                if (valueTemp.contains(WordValueEnum.breakStr.getValue())) {
                    //设置换行
                    String[] text = valueTemp.split(WordValueEnum.breakStr.getValue());
                    //  insertNewRun = paragraph.insertNewRun(0);
                    int length = text.length;
                    //换行符开头的 会丢失
                    if (valueTemp.startsWith(WordValueEnum.breakStr.getValue())) {
                        newRun.addBreak();//硬回车
                    }
                    for (int f = 0; f < length; f++) {
                        if (f == 0) {
                            newRun.setText(text[f]);
                        } else {
                            // 换行
                            newRun.addBreak();//硬回车
                            // insertNewRun.addBreak(BreakType.TEXT_WRAPPING);// 导出word文字强制换行
                            newRun.setText(text[f]);
                        }
                    }
//换行符结束的会丢失
                    if (valueTemp.endsWith(WordValueEnum.breakStr.getValue())) {
                        newRun.addBreak();//硬回车
                    }
                } else {
                    newRun.setText(valueTemp);
                }
                addCount++;

            }
        }

        //在添加的时候 就已经去除多余的站位的了 所以返回的之后 直接返回0 就行了
        return addCount == 0 ? addCount : addCount - 1;
    }

    /**
     * 根据自定义的标签 解析成对象
     * 仅限于字符串类型的渲染
     *
     * @param parameterBean
     * @return
     */
    public static List<ParameterBean> parseValue(ParameterBean parameterBean, WordValueEnum start, WordValueEnum end, BiConsumer<ParameterBean, ParameterBean> yesToDo, BiConsumer<ParameterBean, ParameterBean> noTodo) {
        String startKey = start.getValue();
        String endKey = end.getValue();

        String startKey_escape = StringEscapeUtils.unescapeJava(startKey);
        String endKey_escape = StringEscapeUtils.unescapeJava(endKey);
        String value = StringEscapeUtils.unescapeJava(parameterBean.getValue().toString());


        List<ParameterBean> parameterBeanList = new ArrayList<>();
        if (StringUtils.isBlank(value)) {
            parameterBeanList.add(parameterBean);//传入的空格是需要写入的
            return parameterBeanList;
        }
        //处理是否有下划线
        if (value.contains(startKey_escape)) { //判断是否存在的时候 去除转义符的影响
            Arrays.stream(value.split(startKey)).forEach(s -> {
                if (s.contains(endKey_escape)) {
                    String[] split = s.split(endKey);
                    if (split.length == 0) {
                        parameterBeanList.add(new ParameterBean("none", " ")); //防止切割完成之后 没有 获取失败
                    } else {
                        ParameterBean doSet = new ParameterBean("doSet", split[0]);
                        yesToDo.accept(parameterBean, doSet);
                        parameterBeanList.add(doSet);
                        if (split.length == 2) { //防止切割完成之后 只有一个 获取失败
                            ParameterBean noneTodo = new ParameterBean("none", split[1]);
                            noTodo.accept(parameterBean, noneTodo);
                            parameterBeanList.add(noneTodo);
                        }
                    }
                } else {
                    parameterBeanList.add(new ParameterBean("none", s));
                }
            });

        } else {
            //如果没有标签 就把原始的对象返回回去进行渲染

            ParameterBean aThis = new ParameterBean("this", value);
            BeanUtils.copyProperties(parameterBean, aThis);
            aThis.setKey("this");
            aThis.setValue(value);
            noTodo.accept(parameterBean, aThis); //处理标签中间的代码
            noTodo.accept(parameterBean, aThis);
            parameterBeanList.add(aThis);
        }
        return parameterBeanList;
    }


    /**
     * @param ctGraphicalObject 图片数据
     * @param deskFileName      图片描述
     * @param width             宽
     * @param height            高
     * @param leftOffset        水平偏移 left
     * @param topOffset         垂直偏移 top
     * @param behind            文字上方，文字下方
     * @return
     * @throws Exception
     */
    public static CTAnchor getAnchorWithGraphic(CTGraphicalObject ctGraphicalObject,
                                                String deskFileName, int width, int height,
                                                int leftOffset, int topOffset, boolean behind) {
        String anchorXML =
                "<wp:anchor xmlns:wp=\"http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing\" "
                        + "simplePos=\"0\" relativeHeight=\"0\" behindDoc=\"" + ((behind) ? 1 : 0) + "\" locked=\"0\" layoutInCell=\"1\" allowOverlap=\"1\">"
                        + "<wp:simplePos x=\"0\" y=\"0\"/>"
                        + "<wp:positionH relativeFrom=\"column\">"
                        + "<wp:posOffset>" + leftOffset + "</wp:posOffset>"
                        + "</wp:positionH>"
                        + "<wp:positionV relativeFrom=\"paragraph\">"
                        + "<wp:posOffset>" + topOffset + "</wp:posOffset>" +
                        "</wp:positionV>"
                        + "<wp:extent cx=\"" + width + "\" cy=\"" + height + "\"/>"
                        + "<wp:effectExtent l=\"0\" t=\"0\" r=\"0\" b=\"0\"/>"
                        + "<wp:wrapNone/>"
                        + "<wp:docPr id=\"1\" name=\"Drawing 0\" descr=\"" + deskFileName + "\"/><wp:cNvGraphicFramePr/>"
                        + "</wp:anchor>";

        CTDrawing drawing = null;
        try {
            drawing = CTDrawing.Factory.parse(anchorXML);
        } catch (XmlException e) {
            e.printStackTrace();
        }
        CTAnchor anchor = drawing.getAnchorArray(0);
        anchor.setGraphic(ctGraphicalObject);
        return anchor;
    }

}
