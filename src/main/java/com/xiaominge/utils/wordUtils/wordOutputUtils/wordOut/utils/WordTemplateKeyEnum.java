package com.xiaominge.utils.wordUtils.wordOutputUtils.wordOut.utils;


import lombok.Getter;

/**
 * word 模板特殊key 枚举类
 */
@Getter
public enum WordTemplateKeyEnum {


    //文本类
    textForStart("##{forEachRunstart}##", "段落循环开始标志"),
    textForKeyPrefix("##{forEachKey_", "寻找循环段落的前缀,根据后面的key去textForReaderList 里面找对应的对象"),
    textForKeyPrefixValue("forEachKey_", "循环文字标志"),
    textForEnd("##{forEachRunstEnd}##", "需要循环的段落 结束标志 ,对应开始 中间的段落进行循环"),


    //图片类型
    imageKeyPerfix("image_", "图片key 前缀"),

    //开始行循环标志
    tableRowForEachStart("##{foreachRows}##", "表格内部行循环开始标志"),

    keyStart("{", "key 开始标志"),
    keyEnd("}", "key 结束标志");


    /**
     * 测试代码的模板
     * <p>
     * 农业行政执法基本文书格式三：
     * 询问笔录
     * <p>
     * 询问机关：{deptName}
     * 询问人：{askUser}
     * 图片{image_user}
     * <p>
     * ##{forEachRunstart}##
     * ##{forEachKey_userSing}##
     * 用户名称: {userName}   执法证件号： {cardNumber}
     * ##{forEachRunstEnd}##
     * <p>
     * <p>
     * 表格111
     * ##{foreachTableRow}##	checkTypeLinkTypeMchCountTable
     * 序号	    检查类别        	    检查环节	                        检查对象
     * 抽查对象数量	            检查发现问题的执法对象数量	    占比
     * ##{foreachRows}##
     * {index}    {checkTypeName}        {linkTypeName}    {mchAllCheckNum}    {mchHaveProblemNum}            {ratio}
     * 尾巴	        尾巴	            尾巴	        尾巴	            尾巴	                尾巴
     * <p>
     * <p>
     * 表格2222
     * ##{foreachTable}##	checkTypeLinkTypeMchCountTable2
     * 序号	    检查类别        	    检查环节	                        检查对象
     * 抽查对象数量	            检查发现问题的执法对象数量	    占比
     * ##{foreachRows}##
     * {index}    {checkTypeName}        {linkTypeName}    {mchAllCheckNum}    {mchHaveProblemNum}            {ratio}
     * 尾巴	        尾巴	            尾巴	        尾巴	            尾巴	                尾巴
     * <p>
     * <p>
     * <p>
     * <p>
     * <p>
     * 表格3333
     * ##{replaceValueTable}##	checkTypeLinkTypeMchCountTable3
     * 序号	检查类别	检查环节	检查对象
     * 抽查对象数量	检查发现问题的执法对象数量	占比
     * {index}    {checkTypeName}    {linkTypeName}    {mchAllCheckNum}    {mchHaveProblemNum}    {ratio}
     * 尾巴2	尾巴2	尾巴2	尾巴2	尾巴2	尾巴2
     * <p>
     * <p>
     * 表格4444444
     * 序号	检查类别	检查环节	检查对象
     * 抽查对象数量	检查发现问题的执法对象数量	占比
     * 11	111	11	11	11	11
     */


    private String keyCode;
    private String keyDescDescribe;

    WordTemplateKeyEnum(String keyCode, String keyDescDescribe) {
        this.keyCode = keyCode;
        this.keyDescDescribe = keyDescDescribe;
    }


}
