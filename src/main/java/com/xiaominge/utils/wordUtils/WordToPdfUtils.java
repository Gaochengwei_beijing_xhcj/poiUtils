package com.xiaominge.utils.wordUtils;

import com.aspose.words.Document;
import com.aspose.words.License;
import com.aspose.words.SaveFormat;
import com.xiaominge.exception.ParameterRuntimeException;
import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.*;

/**
 * @program: system_platform
 * @description: word 转pdf
 * @author: xiaominge
 * @create: 2020-12-30 14:50
 **/
@Slf4j
public class WordToPdfUtils {
    private WordToPdfUtils() {
    }

    private static WordToPdfUtils wordToPdfUtils;

    public static WordToPdfUtils getUtils() {
        if (wordToPdfUtils == null) {
            synchronized (WordToPdfUtils.class) {
                if (wordToPdfUtils == null) {
                    wordToPdfUtils = new WordToPdfUtils();
                }
            }
        }
        return wordToPdfUtils;
    }


    /**
     * 多克斯为pdf
     *
     * @param wordInputStream 字输入流
     * @param pdfOutputStream pdf输出流
     *                        当前方法有一个问题 空字符串会被主动加入空格  源码问题
     *                           Chunk chunk = createTextChunk( textContent.isEmpty() ? " " : sbuf.toString(), pageNumber, chunkFont,
     *                                        underlinePatterns, backgroundColor );
     */

    public void docxToPdf(InputStream wordInputStream, OutputStream pdfOutputStream) {
        if (wordInputStream == null) {
            ParameterRuntimeException.throwException("word文件流为空");
        }
        try {
            XWPFDocument xwpfDocument = new XWPFDocument(wordInputStream);
          PdfConverter.getInstance().convert(xwpfDocument, pdfOutputStream, PdfOptions.create());

            wordInputStream.close();
            pdfOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            ParameterRuntimeException.throwException("读取word文件失败,只支持docx文件");
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        WordToPdfUtils utils = WordToPdfUtils.getUtils();
       // utils.docxToPdf(new FileInputStream("D:/桌面/卷内文件1002247741864148992.docx"), new FileOutputStream("D:/桌面/test.pdf"));
        utils.conversion(new FileInputStream("D:\\Backup\\Documents\\WeChat Files\\wx1360835988\\FileStorage\\File\\2023-03\\询问笔录-2023年03月16日14时55分53秒.docx") ,
                new FileOutputStream("D:\\Backup\\Documents\\WeChat Files\\wx1360835988\\FileStorage\\File\\2023-03\\询问笔录-2023年03月16日14时55分53秒.pdf"),
                SaveFormat.PDF
                );
    }

    public  boolean getLicense() {
        boolean result = false;
        try {
            // license.xml应放在..\WebRoot\WEB-INF\classes路径下
            InputStream is = this.getClass().getClassLoader().getResourceAsStream("license.xml");
            License aposeLic = new License();
            aposeLic.setLicense(is);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 转换
     * // 全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
     *
     * @param saveTo       SaveFormat {@link SaveFormat }
     * @param inputStream  输入流
     * @param outputStream 输出流
     */
    public  void conversion(InputStream inputStream, OutputStream outputStream,int saveTo ) {
        // 验证License 若不验证则转化出的pdf文档会有水印产生
        if (!getLicense()) {
            return;
        }
        try {
            // Address是将要被转化的word文档
            Document doc = new Document(inputStream);
            // 全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
            doc.save(outputStream, saveTo);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            throw ParameterRuntimeException.throwException("word转pdf失败");
        }
    }

}
