package com.xiaominge.utils.excleUtils.xlsAndxlsx.excelMerge;

import lombok.Getter;
import lombok.Setter;

/**
 * @program: parent
 * @description: excel合并单元格数据对象类
 * @author: xiaominge
 * @create: 2019-09-10 09:43
 * <p>
 * 分别为起始行，结束行，起始列，结束列)
 * // 行和列都是从0开始计数，且起始结束都会合并
 **/
@Getter
@Setter
public class ExcelMergeData {


    /**
     * 起始行
     */
    private Integer startRowIndex;
    /**
     * 结束行
     */
    private Integer endRowIndex;

    /**
     * 合并多少行  0 代表不合并
     */
    private Integer MergeRowNum;

    /**
     * 起始列 起始列从1开始算起  excel读取的时候 cell 是从0 开始读取的
     */
    private Integer startCollIndex;
    /**
     * 结束列
     */
    private Integer endCollIndex;

    public ExcelMergeData(Integer mergeRowNum, Integer startCollIndex, Integer endCollIndex) {

        this.MergeRowNum = mergeRowNum;
        this.startCollIndex = startCollIndex;
        this.endCollIndex = endCollIndex;
    }
}
