package com.xiaominge.utils.excleUtils.xlsAndxlsx.excelOut.web;

import com.xiaominge.exception.ParameterRuntimeException;
import com.xiaominge.utils.excleUtils.xlsAndxlsx.excelOut.ExcelData;
import com.xiaominge.utils.excleUtils.xlsAndxlsx.excelOut.ExcelOutputUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

/**
 * @program: case_system_parent
 * @description: 网页版
 * @author: Administrator
 * @create: 2020-07-28 16:23
 **/


public class ExcelOutWeb<T> {

    public void exportExcel(XSSFWorkbook wb,
                            List<T> dataList,
                            List<ExcelWebBean> excelWebBeans,
                            int sheetIndex,
                            String sheetName,
                            int rowIndex


    ) throws Exception {
        if (excelWebBeans == null || excelWebBeans.size() == 0) {
            throw ParameterRuntimeException.throwException("请选择需要导出的数据");
        }
        ExcelData excelData = new ExcelData(sheetIndex, StringUtils.isBlank(sheetName)?"sheet1":sheetName,rowIndex);

        List<String> title = excelData.CreateTitle();
        List<String> fieldName = new ArrayList<>();
        for (ExcelWebBean excelWebBean : excelWebBeans) {
            fieldName.add(excelWebBean.getFieldName());
            title.add(excelWebBean.getFieldDesc());
        }
        //获取内容
        for (T one : dataList) {
            Field[] fields = one.getClass().getDeclaredFields();
            ConcurrentMap<String, Field> nameToFieldMap = Arrays.stream(fields).collect(Collectors.toConcurrentMap(Field::getName, field -> field));
            List<ExcelData.CellDataInfo> oneRow = excelData.createOneRow();
            for (String fileName : fieldName) {
                ExcelData.CellDataInfo oneCell = excelData.getCellDataInfo();
                Field field = nameToFieldMap.get(fileName);
                field.setAccessible(true);
                Class fieldType = field.getType();
                boolean primitive = fieldType.isPrimitive();

                if (primitive
                        || fieldType.getPackage().getName().equals("java.lang")
                        || fieldType.getPackage().getName().equals("java.util")) {
                    Object filedValue = field.get(one);

                    oneCell.setData(filedValue);
                    oneRow.add(oneCell);
                } else {
                    throw ParameterRuntimeException.throwException("自定义类型的对象不能直接写入excel");
                }
            }
        }
        ExcelOutputUtils.writerToExcel(wb, excelData);
    }


    public void exportExcel(OutputStream outputStream, List<T> dataList, List<ExcelWebBean> excelWebBeans) throws Exception {
        XSSFWorkbook wb = new XSSFWorkbook();
        exportExcel(wb, dataList, excelWebBeans,0,"sheet1",0);
        wb.write(outputStream);
    }

    /**
     *
     * @param dataList 数据
     * @param excelWebBeans 字段名对应描述
     * @param sheetIndex sheet index
     * @param sheetName sheetName
     * @param rowIndex 开始行 的序号 第一行是0
     * @param response
     * @param fileName 文件名
     * @throws Exception
     */
    public void exportExcel(List<T> dataList, List<ExcelWebBean> excelWebBeans, int sheetIndex,
                            String sheetName,
                            int rowIndex, HttpServletResponse response, String fileName) throws Exception {
        // 告诉浏览器用什么软件可以打开此文件
        response.setHeader("content-Type", "application/vnd.ms-excel");
        // 下载文件的默认名称
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "utf-8"));
        XSSFWorkbook wb = new XSSFWorkbook();
        exportExcel(wb, dataList, excelWebBeans,sheetIndex, sheetName,rowIndex);
        ServletOutputStream outputStream = response.getOutputStream();
        wb.write(outputStream);
        outputStream.close();
        //  wb.close();
    }

    public void exportExcel(List<T> dataList, List<ExcelWebBean> excelWebBeans, HttpServletResponse response, String fileName) throws Exception {
        // 告诉浏览器用什么软件可以打开此文件
        response.setHeader("content-Type", "application/vnd.ms-excel");
        // 下载文件的默认名称
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "utf-8"));
        XSSFWorkbook wb = new XSSFWorkbook();
        exportExcel(wb, dataList, excelWebBeans,0,"sheet1",0);
        ServletOutputStream outputStream = response.getOutputStream();
        wb.write(outputStream);
        outputStream.close();
        //  wb.close();
    }
/*

    public void test() throws Exception {
        List<UserInfo> userInfos = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            if (i / 2 == 0) {
                UserInfo userInfo = new UserInfo("name" + i, i, 1);
                userInfos.add(userInfo);
            } else {
                UserInfo userInfo = new UserInfo("name" + i, i, 0);
                userInfos.add(userInfo);
            }
        }
        List<ExcelWebBean> clou = new ArrayList() {{
            add(new ExcelWebBean("userName", "用户名称"));
            add(new ExcelWebBean("idCard", "身份证号码"));
            add(new ExcelWebBean("sex", "性别"));
        }};

        ExcelOutWeb excelOutWeb = new ExcelOutWeb();
        XSSFWorkbook wb = new XSSFWorkbook();

        excelOutWeb.exportExcel(wb,
                userInfos,
                clou);
        wb.write(new FileOutputStream("D:/桌面/test.xlsx"));
    }

    public static void main(String[] args) throws Exception {
        ExcelOutWeb excelOutWeb = new ExcelOutWeb();
        excelOutWeb.test();

    }

    @Data
    @AllArgsConstructor
    public static class UserInfo {
        @ExcelTitleName("名称")
        private String userName;

        @ExcelTitleName("身份证")
        private int idCard;

        private int sex;
    }
*/

}
