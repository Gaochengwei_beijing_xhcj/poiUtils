package com.xiaominge.utils.excleUtils.xlsAndxlsx.excelOut;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;


/**
 * @Author xiaomin
 * @Description
 * @Date 2019/5/9 9:20
 */
public class ExcelOutputUtils {

    /**
     * 相应页面
     *
     * @param response
     * @param wb
     * @param fileName
     * @param data
     * @throws Exception
     */
    public static void exportExcel(HttpServletResponse response, Workbook wb, String fileName, ExcelData data) throws Exception {
        // 告诉浏览器用什么软件可以打开此文件
        response.setHeader("content-Type", "application/vnd.ms-excel");
        // 下载文件的默认名称
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "utf-8"));
        //写入数据  并输出
        exportExcel(wb, data, response.getOutputStream());
    }


    /**
     * 写出到指定流里面
     *
     * @param wb
     * @param data
     * @param out
     * @throws Exception
     */

    public static void exportExcel(Workbook wb, ExcelData data, OutputStream out) throws Exception {
        //创建一个工作薄  格式 xlsx
        // Workbook wb = new Workbook();

        //Workbook wb = new HSSFWorkbook();  // 格式 xls
        Sheet sheet=null;
        try {
            //创建一个一页 如果有就直获取 没有就创建
            //创建一个一页 如果有就直获取 没有就创建
            int numberOfNames = wb.getNumberOfNames();
            if (numberOfNames > data.getSheetIndex()) {
                sheet = wb.getSheetAt(data.getSheetIndex());
            }
            if (sheet == null) {
                sheet = wb.getSheet(data.getSheetName());
            }
            if (sheet == null) {
                sheet = wb.createSheet();
                wb.setSheetName(data.getSheetIndex(), data.getSheetName());
            }
            //写入数据
            ExcelWriteUtils.writeExcel(wb, sheet, data);

            wb.write(out);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
            if (sheet == null) {
                 wb.createSheet();
                wb.setSheetName(data.getSheetIndex(), data.getSheetName());
            }
        } finally {
            //此处需要关闭 wb 变量
            // wb.close();
            out.close();
        }
    }


    /**
     * 只是写入数据 并没有 进行输出
     *
     * @param wb
     * @param data
     * @return
     */
    public static Workbook writerToExcel(Workbook wb, ExcelData data) {
        //页签的名称
        //创建一个一页 如果有就直获取 没有就创建
        Sheet sheet = wb.getSheetAt(data.getSheetIndex());
        if (sheet == null) {
            sheet = wb.getSheet(data.getSheetName());
            if (sheet == null) {
                sheet = wb.createSheet();
            //在这里设置名字的原因 自己创建的sheet
            wb.setSheetName(data.getSheetIndex(), data.getSheetName());
            }
        }
        //写入数据
        ExcelWriteUtils.writeExcel(wb, sheet, data);
        return wb;
    }


}
