package com.xiaominge.utils.excleUtils.xlsAndxlsx.excelOut.template;

import com.xiaominge.exception.ParameterRuntimeException;
import com.xiaominge.utils.excleUtils.xlsAndxlsx.excelMerge.ExcelDataAll;
import com.xiaominge.utils.excleUtils.xlsAndxlsx.excelOut.ExcelData;
import lombok.Getter;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: system_platform
 * @description: 模板导出 bean 对象
 * @author: xiaominge
 * @create: 2021-04-13 17:16
 **/

@Getter
public class TemplateBean {

    /**
     * 模板的输入流
     */
    private InputStream templateStream;

    private Workbook wb;


    /**
     * 没有合并单元格的数据导出
     */
    private List<ExcelData> excelDataList;

    /**
     * 合并单元格的数据导出
     */
    private List<ExcelDataAll> excelDataAllList;


    public static TemplateBean getTemplateBean(InputStream templateStream) {
        if (templateStream == null) {
            throw new RuntimeException("模板流不能为空");
        }
        return new TemplateBean(templateStream);
    }

    public static TemplateBean getTemplateBean(String templateFilePath) {
        InputStream templateStream;
        try {
            templateStream = new FileInputStream(templateFilePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw ParameterRuntimeException.throwException("template  FileNotFound");
        }
        return new TemplateBean(templateStream);
    }

    /**
     * 构造读取模板
     *
     * @param templateStream
     */
    private TemplateBean(InputStream templateStream) {
        this.templateStream = templateStream;
        try {
            this.wb =  create(templateStream);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("读取模板失败");
        } catch (InvalidFormatException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("模板当前只支持xlsx格式的");
        }
    }


    public static Workbook create(InputStream inp) throws IOException, InvalidFormatException {
        //直接创建
        return WorkbookFactory.create(inp);
    }

    //一下是没有合并单元的模板数据导出

    /**
     * 创建sheet
     *
     * @param sheetIndex sheet Index
     * @param sheetName  sheet名称
     * @param rowIndex   开始写的行数
     * @return
     */
    public  synchronized  ExcelData  createOneSheet(Integer sheetIndex,
                                    String sheetName,
                                    int rowIndex) {

        if (this.excelDataList == null) {
            this.excelDataList = new ArrayList<>();
        }
        List<Integer> sheetNums = excelDataList.stream().map(ExcelData::getSheetIndex).collect(Collectors.toList());
        List<String> sheetNames = excelDataList.stream().map(ExcelData::getSheetName).collect(Collectors.toList());

        if (sheetNums.contains(sheetIndex) || sheetNames.contains(sheetName)) {
            throw new IllegalArgumentException(sheetIndex+"-"+sheetName+"-sheet名称或者序号已经存在");
        }
        ExcelData excelData = new ExcelData(sheetIndex,sheetName,rowIndex);
        excelData.setRowIndex(rowIndex);
        excelData.setSheetIndex(sheetIndex);
        excelData.setSheetName(sheetName);
        excelData.CreateTitle(); //创建空的标题行
        this.excelDataList.add(excelData);
        return excelData;
    }


    //一下是有合并单元格的模板数据导出
    public ExcelDataAll createOneSheet(Integer sheetIndex, String sheetName) {
        if (this.excelDataAllList == null) {
            this.excelDataAllList = new ArrayList<>();
        }
        List<Integer> sheetNums = excelDataAllList.stream().map(ExcelDataAll::getSheetNum).collect(Collectors.toList());
        List<String> sheetNames = excelDataAllList.stream().map(ExcelDataAll::getSheetname).collect(Collectors.toList());

        if (sheetNums.contains(sheetIndex) || sheetNames.contains(sheetName)) {
            throw new IllegalArgumentException("sheet名称或者序号已经存在");
        }
        ExcelDataAll excelDataAll = new ExcelDataAll();
        excelDataAll.setSheetNum(sheetIndex);
        excelDataAll.setSheetname(sheetName);
        excelDataAll.setTitles(new ArrayList<>()); //创建空的标题行
        this.excelDataAllList.add(excelDataAll);
        return excelDataAll;
    }

}
