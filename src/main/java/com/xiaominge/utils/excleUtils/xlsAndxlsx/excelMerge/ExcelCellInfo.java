package com.xiaominge.utils.excleUtils.xlsAndxlsx.excelMerge;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @program: parent
 * @description: 单元格cell 的坐标位置
 * @author: xiaominge
 * @create: 2019-09-10 09:46
 **/
@Getter
@Setter
@Accessors(chain = true)
public class ExcelCellInfo {

    /**
     * 单元格 内容
     */
    private String content;
    /**
     * 单元格的高度
     */
    private Integer height;
    /**
     * 图片流的字节数组
     */
    private byte[] imageBytes;
    /**
     * Workbook.PICTURE_TYPE_JPEG
     */
    private int imageType;

    /**
     * 单元格行号
     */
    private Integer rowIndex;
    /**
     * 单元格列号
     */
    private Integer collIndex;

    /**
     * 单元格合并信息
     */
    private ExcelMergeData excelMergeData;

    public ExcelCellInfo(String content) {
        this.content = content;
    }

    /**
     * 创建图片导出构造
     * @param imageBytes
     * @param imageType
     */
    public ExcelCellInfo(byte[] imageBytes, int imageType) {
        this.imageBytes = imageBytes;
        this.imageType = imageType;
    }

    public ExcelCellInfo(String content, Integer collIndex) {
        this.content = content;
        this.collIndex = collIndex;

    }
    /**
     * 创建图片导出构造
     *
     * @param imageBytes
     * @param imageType
     */
    public ExcelCellInfo(byte[] imageBytes, int imageType, Integer collIndex) {
        this.imageBytes = imageBytes;
        this.imageType = imageType;
        this.collIndex = collIndex;
    }

    public ExcelCellInfo(ByteArrayOutputStream byteArrayOutputStream, int imageType) {
        this(byteArrayOutputStream.toByteArray(), imageType);
    }

    public ExcelCellInfo(ByteArrayOutputStream byteArrayOutputStream, int imageType, Integer collIndex) {
        this(byteArrayOutputStream.toByteArray(), imageType, collIndex);
    }

    public ExcelCellInfo(InputStream input, int imageType) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024 * 4];
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
        }
        this.imageBytes = output.toByteArray();
        this.imageType = imageType;
    }


    public ExcelCellInfo(String content, Integer collIndex, ExcelMergeData excelMergeData) {
        this.content = content;
        this.collIndex = collIndex;
        this.excelMergeData = excelMergeData;
    }

    public ExcelCellInfo(String content, ExcelMergeData excelMergeData) {
        this.content = content;
        this.excelMergeData = excelMergeData;
    }
}
