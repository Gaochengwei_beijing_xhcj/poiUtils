package com.xiaominge.utils.excleUtils.xlsAndxlsx.excelOut.web;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @program: case_system_parent
 * @description:
 * @author: Administrator
 * @create: 2020-07-28 16:25
 **/


@Getter
@AllArgsConstructor
public class ExcelWebBean {
    private String fieldName;

    private String fieldDesc;


}
