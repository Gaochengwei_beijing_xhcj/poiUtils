package com.xiaominge.utils.excleUtils.xlsAndxlsx.excelOut.template;

import com.xiaominge.exception.ParameterRuntimeException;
import com.xiaominge.utils.excleUtils.xlsAndxlsx.excelMerge.ExcelCellInfo;
import com.xiaominge.utils.excleUtils.xlsAndxlsx.excelMerge.ExcelDataAll;
import com.xiaominge.utils.excleUtils.xlsAndxlsx.excelMerge.ExcelMergeOutputUtils;
import com.xiaominge.utils.excleUtils.xlsAndxlsx.excelMerge.ExcelRowInfo;
import com.xiaominge.utils.excleUtils.xlsAndxlsx.excelOut.ExcelData;
import com.xiaominge.utils.excleUtils.xlsAndxlsx.excelOut.ExcelWriteUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;

/**
 * @program: system_platform
 * @description: 模板工具
 * @author: xiaominge
 * @create: 2021-04-13 17:15
 **/
@Slf4j
public class TemplateUtils {

    /**
     * @param templateBean  模板信息
     * @param outputStreams
     * @throws IOException
     */
    public static void exportExcel(TemplateBean templateBean, OutputStream... outputStreams) {
        if (outputStreams == null || outputStreams.length == 0) {
            throw new IllegalArgumentException("输出流不能为空");
        } else {
            if (outputStreams.length > 2) {
                ParameterRuntimeException.throwException("输出流最多只能设置2个");
            }
        }
        if (templateBean == null) {
            throw new IllegalArgumentException("导出数据不能为空");
        }
        List<ExcelData> excelDataList = templateBean.getExcelDataList();

        List<ExcelDataAll> excelDataAllList = templateBean.getExcelDataAllList();

        if (excelDataList != null && !excelDataList.isEmpty() && excelDataAllList != null && !excelDataAllList.isEmpty()) {
            if (outputStreams.length != 2) {
                throw ParameterRuntimeException.throwException("两个数据导出 设置的输出流必须是两个");
            }
        }
        if (excelDataAllList == null && excelDataList == null) {
            throw ParameterRuntimeException.throwException("导出数据不能为空");
        }

        OutputStream out = outputStreams[0];

        Workbook wb = templateBean.getWb();
        //导出 没有合并的
        if (excelDataList != null) {
            for (ExcelData excelData : excelDataList) {
                try {
                    //写入数据
                    ExcelWriteUtils.writerToExcel(wb, excelData);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw ParameterRuntimeException.throwException("excel 不合并单元格导出失败");
                }
            }
        }
        //导出合并单元格版本的
        if (excelDataAllList != null) {
            for (ExcelDataAll excelDataAll : excelDataAllList) {
                ExcelMergeOutputUtils.writeExcel(wb, excelDataAll);
            }
        }

        try {
            wb.write(out);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //此处需要关闭 wb 变量
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void exportExcel(HttpServletResponse response, String fileName, TemplateBean templateBean) throws Exception {
        // 告诉浏览器用什么软件可以打开此文件
        response.setHeader("content-Type", "application/vnd.ms-excel");
        // 下载文件的默认名称
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "utf-8"));
        //写入数据  并输出
        if ((templateBean.getExcelDataList() != null && !templateBean.getExcelDataList().isEmpty())
                && (templateBean.getExcelDataAllList() != null && !templateBean.getExcelDataAllList().isEmpty())) {
            throw ParameterRuntimeException.throwException("response 只有一个输出流,不能同时导出多个文件");
        }
        exportExcel(templateBean, response.getOutputStream());
    }



    //测试
    public static void main(String[] args) throws FileNotFoundException {
        String xls = "D:\\桌面\\附表1：执法机构数据导出格式.xls";

        String xlsx = "D:\\桌面\\temp\\附表1：执法机构数据导出格式.xlsx";


        TemplateBean templateBean = TemplateBean.getTemplateBean(xlsx);
        ExcelDataAll sheet = templateBean.createOneSheet(3, "渔政码头信息");
        for (int i = 0; i < 10; i++) {
            ExcelRowInfo oneRow = sheet.createOneRow(i + 2);
            for (int j = 0; j < 5; j++) {
                oneRow.addOneCell(new ExcelCellInfo("cellINfo-"+i));
            }
        }
        File out = new File("D:\\桌面\\temp\\test-执法机构数据导出格式.xlsx");
        try {
            exportExcel(templateBean, new FileOutputStream(out));
        } catch (RuntimeException mr) {
            log.info(mr.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            if (out.exists()) {
                out.delete();
            }
        }
    }
}
