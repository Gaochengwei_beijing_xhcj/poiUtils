package com.xiaominge.utils.excleUtils.xlsAndxlsx.excelMerge;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: parent
 * @description: 一行数据信息
 * @author: xiaominge
 * @create: 2019-09-10 10:52
 **/

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExcelRowInfo {

    /**
     * 行号
     */
    Integer rowIndex;
    /**
     * 一行数据
     */
    List<ExcelCellInfo> excelCellInfos;

    public List<ExcelCellInfo> addOneCell(ExcelCellInfo excelCellInfo) {
        if (excelCellInfo == null) {
            throw new RuntimeException("cell 单元格内容不能为空");
        }
        if (this.excelCellInfos == null) {
            this.excelCellInfos = new ArrayList<>();
        }
        this.excelCellInfos.add(excelCellInfo);
        return this.excelCellInfos;
    }
}
