package com.xiaominge.utils.excleUtils.xlsAndxlsx.excelMerge;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: parent
 * @description: excel 数据对象
 * @author: xiaominge
 * @create: 2019-09-10 09:49
 **/
public class ExcelDataAll {

    /**
     * 所有 标题  里面是一行标题
     * 根据标题里面是否有合并单元格信息 进行合并单元格.
     */
    List<ExcelRowInfo> titles;
    /**
     * 所有行数据  里面是一行 数据
     */
    List<ExcelRowInfo> rows;
    /**
     * 页签的数
     */
    private Integer SheetNum = 0;
    /**
     * 页签名称
     */
    private String Sheetname;

    /**
     * 所有需要合并单元格的信息  在数据渲染完成之后进行合并单元格
     */
   private  List<ExcelMergeData> allexExcelMergeData = new ArrayList<>();

    /**
     * 行是从0开始计算的
     * @param rowIndex
     * @return
     */
    public ExcelRowInfo createOneRow(Integer rowIndex) {
        if (rows == null) {
            rows = new ArrayList<>();
        }
        ExcelRowInfo oneRow = new ExcelRowInfo();
        oneRow.setRowIndex(rowIndex);
        List<ExcelCellInfo> rowCell = new ArrayList<>();
        oneRow.setExcelCellInfos(rowCell);
        rows.add(oneRow);
        return oneRow;
    }


    public List<ExcelRowInfo> getTitles() {
        return titles;
    }

    public void setTitles(List<ExcelRowInfo> titles) {
        this.titles = titles;
    }

    public List<ExcelRowInfo> getRows() {
        return rows;
    }

    public void setRows(List<ExcelRowInfo> rows) {
        this.rows = rows;
    }

    public Integer getSheetNum() {
        return SheetNum;
    }

    public void setSheetNum(Integer sheetNum) {
        SheetNum = sheetNum;
    }

    public String getSheetname() {
        return Sheetname;
    }

    public void setSheetname(String sheetname) {
        Sheetname = sheetname;
    }

    public List<ExcelMergeData> getAllexExcelMergeData() {
        return allexExcelMergeData;
    }

    public void setAllexExcelMergeData(List<ExcelMergeData> allexExcelMergeData) {
        this.allexExcelMergeData = allexExcelMergeData;
    }
}
