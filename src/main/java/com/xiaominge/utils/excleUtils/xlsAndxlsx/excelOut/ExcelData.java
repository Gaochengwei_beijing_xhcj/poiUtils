package com.xiaominge.utils.excleUtils.xlsAndxlsx.excelOut;

import com.xiaominge.exception.ParameterRuntimeException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author xiaomin
 * @Description
 * @Date 2019/5/9 9:18
 */
@Getter
@Setter
public class ExcelData implements Serializable {
    private static final long serialVersionUID = 4444017239100620999L;

    // 表头
    private List<String> titles;

    //页签序号

    private int SheetIndex;

    // 页签名称
    private String SheetName ;

    //数据开始写的行 不是标题
    private Integer rowIndex;

    // 数据
    private List<List<CellDataInfo>> rows = new ArrayList<>();


    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public List<String> CreateTitle() {
        this.titles = new ArrayList<>();
        return titles;
    }


    public List<CellDataInfo> createOneRow() {
        if (this.rows == null) {
            this.rows = new ArrayList<>();
        }
        List<CellDataInfo> oneRow = new ArrayList<>();
        rows.add(oneRow);
        return oneRow;
    }

    public CellDataInfo getCellDataInfo() {
        return new CellDataInfo();
    }

    public CellDataInfo getCellDataInfo(Object cellData) {
        return new CellDataInfo(cellData);
    }

    public CellDataInfo createOneCell() {
        return new CellDataInfo();
    }

    public CellDataInfo createOneCell(Object cellData) {
        return new CellDataInfo(cellData);
    }


    public ExcelData(int sheetIndex, String sheetName, Integer rowIndex) {
        if (sheetIndex < 0) {
            ParameterRuntimeException.throwException("sheet index 不能小于0");
        }
        SheetIndex = sheetIndex;

        if (StringUtils.isBlank(sheetName)) {
            sheetName = "sheet1";
        }
        SheetName = sheetName;
        this.rowIndex = rowIndex;
    }

    @Getter
    @NoArgsConstructor
    public class CellDataInfo {

        // 新版不需要这个类型了
        // private int cellType = HSSFCell.CELL_TYPE_STRING; //现在全部设置为string 类型  后序
        private Object data;


        public void setData(Object data) {
            this.data = data;
        }


        public CellDataInfo(Object data) {
            this.data = data;
        }
    }


}
