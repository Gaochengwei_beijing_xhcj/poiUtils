package com.xiaominge.utils.excleUtils.excelOut;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder.BorderSide;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @Author xiaomin
 * @Description
 * @Date 2019/5/9 9:20
 */
public class ExcelWriteUtils {

    /**
     * 相应页面
     *
     * @param response
     * @param wb
     * @param fileName
     * @param data
     * @throws Exception
     */
    public static void exportExcel(HttpServletResponse response, XSSFWorkbook wb, String fileName, ExcelData data) throws Exception {
        // 告诉浏览器用什么软件可以打开此文件
        response.setHeader("content-Type", "application/vnd.ms-excel");
        // 下载文件的默认名称
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "utf-8"));
        //写入数据  并输出
        exportExcel(wb, data, response.getOutputStream());
    }


    /**
     * 写出到指定流里面
     *
     * @param wb
     * @param data
     * @param out
     * @throws Exception
     */

    public static void exportExcel(XSSFWorkbook wb, ExcelData data, OutputStream out) throws Exception {
        //创建一个工作薄  格式 xlsx
        // XSSFWorkbook wb = new XSSFWorkbook();

        //Workbook wb = new HSSFWorkbook();  // 格式 xls
        try {
            //页签的名称
            String sheetName = data.getSheetName();
            if (null == sheetName) {
                sheetName = "Sheet1";
            }
            //创建一个一页
            XSSFSheet sheet = wb.createSheet();
            wb.setSheetName(data.getSheetNum(), sheetName);
            //写入数据
            writeExcel(wb, sheet, data);

            wb.write(out);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //此处需要关闭 wb 变量
            //wb.close();
            out.close();
        }
    }


    /**
     * 只是写入数据 并没有 进行输出
     *
     * @param wb
     * @param data
     * @return
     */
    public static XSSFWorkbook writerToExcel(XSSFWorkbook wb, ExcelData data) {
        //页签的名称
        String sheetName = data.getSheetName();
        if (null == sheetName) {
            throw new RuntimeException("页签名称不能为空");
        }
        //创建一个表格
        XSSFSheet sheet = wb.getSheet(sheetName);
        if (sheet == null) {
            sheet = wb.createSheet();
            wb.setSheetName(data.getSheetNum(), sheetName);
        }

        //写入数据
        writeExcel(wb, sheet, data);
        return wb;
    }


    /**
     * 给指定页签写入数据
     *
     * @param wb    excle 对象
     * @param sheet 页签对象
     * @param data  数据
     */
    public static Workbook writeExcel(Workbook wb, Sheet sheet, ExcelData data) {

        int rowIndex = 0;

        //设置标题  返回行号
        if (data.getTitles() != null && data.getTitles().size() > 0) {
            rowIndex = writeTitlesToExcel(wb, sheet, data.getTitles());
        }
        //写入对象  返回行号
        if (data.getRowIndex() != null) {
            rowIndex = data.getRowIndex();
        }

        writeRowsToExcel(wb, sheet, data.getRows(), rowIndex);
        //自适应宽度设置
        autoSizeColumns(sheet, data.getTitles()==null?1:data.getTitles().size() + 1);
        return wb;

    }

    /**
     * @param wb
     * @param sheet
     * @param titles 写第一行的标题;
     * @return
     */
    private static int writeTitlesToExcel(Workbook wb, Sheet sheet, List<String> titles) {
        int rowIndex = 0;
        int colIndex = 0;
        //创标题样式  建样式对象
        Font titleFont = wb.createFont();
        titleFont.setFontName("Lucida Bright");
        titleFont.setBold(true);
        titleFont.setFontHeightInPoints((short) 14);
        titleFont.setColor(IndexedColors.BLACK.index);

        // 创建单元格样式
        CellStyle titleStyle = wb.createCellStyle();
        titleStyle.setAlignment(HorizontalAlignment.CENTER);
        titleStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        // titleStyle.setFillForegroundColor(new XSSFColor(new Color(182, 184, 192)));
        titleStyle.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());// 设置背景色
        titleStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        titleStyle.setFont(titleFont);
        titleStyle.setWrapText(true);

        setBorder(titleStyle, BorderStyle.THIN,IndexedColors.BLACK);

        Row titleRow = sheet.createRow(rowIndex);
        // titleRow.setHeightInPoints(25);

        for (String field : titles) {
            Cell cell = titleRow.createCell(colIndex);
            cell.setCellValue(field);
            cell.setCellStyle(titleStyle);
            colIndex++;
        }

        rowIndex++;
        return rowIndex;
    }

    /**
     * 数据写入
     *
     * @param wb       excle 对象
     * @param sheet    页签
     * @param rows     数据对象
     * @param rowIndex 开始行号
     * @return
     */
    private static int writeRowsToExcel(Workbook wb, Sheet sheet, List<List<ExcelData.CellDataInfo>> rows, int rowIndex) {
        int colIndex = 0;

        Font dataFont = wb.createFont();
        dataFont.setFontName("Lucida Bright"); //设置字体
        dataFont.setFontHeightInPoints((short) 14); //设置字体大小
        dataFont.setColor(IndexedColors.BLACK.index);

        CellStyle dataStyle = wb.createCellStyle();
        dataStyle.setAlignment(HorizontalAlignment.CENTER);
        dataStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        dataStyle.setFont(dataFont);
        dataStyle.setWrapText(true);
        setBorder(dataStyle, BorderStyle.THIN,IndexedColors.BLACK);
        //创建一行;
       if(rows!=null){
           for (List<ExcelData.CellDataInfo> rowData : rows) {
               Row dataRow = sheet.createRow(rowIndex);
               // dataRow.setHeightInPoints(25);
               colIndex = 0;
               //创建单元格
               for (ExcelData.CellDataInfo cellData : rowData) {
                   Cell cell = dataRow.createCell(colIndex);
                   if (cellData != null) {
                       if (cellData.getData() != null) {
                           //判断data 类型 进行强转进行设置
                           String dataTypeName = cellData.getData().getClass().getName();
                           if (Objects.equals(dataTypeName, String.class.getName())) {
                               cell.setCellValue((String) cellData.getData());
                           } else if (Objects.equals(dataTypeName, Integer.class.getName()) || Objects.equals(dataTypeName, int.class.getName())) {
                               cell.setCellValue((Integer) cellData.getData());
                           } else if (Objects.equals(dataTypeName, Boolean.class.getName()) || Objects.equals(dataTypeName, boolean.class.getName())) {
                               cell.setCellValue((Boolean) cellData.getData());
                           } else if (Objects.equals(dataTypeName, Double.class.getName()) || Objects.equals(dataTypeName, double.class.getName())) {
                               cell.setCellValue((Double) cellData.getData());
                           } else if (Objects.equals(dataTypeName, Date.class.getName())) {
                               cell.setCellValue((Date) cellData.getData());
                           } else if (Objects.equals(dataTypeName, LocalDate.class.getName())) {
                               cell.setCellValue((LocalDate) cellData.getData());
                           } else if (Objects.equals(dataTypeName, LocalDateTime.class.getName())) {
                               cell.setCellValue((LocalDateTime) cellData.getData());
                           } else if (Objects.equals(dataTypeName, Calendar.class.getName())) {
                               cell.setCellValue((Calendar) cellData.getData());
                           } else if (Objects.equals(dataTypeName, RichTextString.class.getName())) {
                               cell.setCellValue((RichTextString) cellData.getData());
                           } else {
                               cell.setCellValue(cellData.getData().toString());
                           }
                       }
                   } else {
                       cell.setCellValue("");
                   }

                   cell.setCellStyle(dataStyle);
                   colIndex++;
               }
               rowIndex++;
           }
       }
        return rowIndex;
    }


    /**
     * 设置宽度
     *
     * @param sheet
     * @param columnNumber
     */
    private static void autoSizeColumns(Sheet sheet, int columnNumber) {

        for (int i = 0; i < columnNumber; i++) {
            int orgWidth = sheet.getColumnWidth(i);
            // 调整每一列宽度
            sheet.autoSizeColumn(i, true);
            // 解决自动设置列宽中文失效的问题
            int newWidth = (int) (sheet.getColumnWidth(i) + 100);  //+100---->* 17 / 10
            int maxWith = 256*255;
            //限制下最大宽度
            if(newWidth > maxWith) {
                sheet.setColumnWidth(i, maxWith);
            }else if (newWidth > orgWidth) {
                sheet.setColumnWidth(i, newWidth);
            } else {
                sheet.setColumnWidth(i, orgWidth);
            }
        }
    }

    /**
     * 设置样式
     *
     * @param style
     * @param border
     * @param color
     */
    private static void setBorder(XSSFCellStyle style, BorderStyle border, XSSFColor color) {
        style.setBorderTop(border);
        style.setBorderLeft(border);
        style.setBorderRight(border);
        style.setBorderBottom(border);
        style.setBorderColor(BorderSide.TOP, color);
        style.setBorderColor(BorderSide.LEFT, color);
        style.setBorderColor(BorderSide.RIGHT, color);
        style.setBorderColor(BorderSide.BOTTOM, color);
    }

    /**
     * 设置样式
     *
     * @param style
     * @param border
     * @param color
     */
    private static void setBorder(CellStyle style, BorderStyle border, IndexedColors color) {

        style.setBorderTop(border);
        style.setBorderLeft(border);
        style.setBorderRight(border);
        style.setBorderBottom(border);
        style.setBottomBorderColor(color.getIndex());
        style.setTopBorderColor(color.getIndex());
        style.setLeftBorderColor(color.getIndex());
        style.setRightBorderColor(color.getIndex());
    }

}
