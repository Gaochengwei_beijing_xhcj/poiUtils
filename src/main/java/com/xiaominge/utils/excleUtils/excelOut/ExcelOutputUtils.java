package com.xiaominge.utils.excleUtils.excelOut;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;


/**
 * @Author xiaomin
 * @Description
 * @Date 2019/5/9 9:20
 */
public class ExcelOutputUtils {

    /**
     * 相应页面
     *
     * @param response
     * @param wb
     * @param fileName
     * @param data
     * @throws Exception
     */
    public static void exportExcel(HttpServletResponse response, Workbook wb, String fileName, ExcelData data) throws IOException {
        // 告诉浏览器用什么软件可以打开此文件
        response.setHeader("content-Type", "application/vnd.ms-excel");
        // 下载文件的默认名称
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "utf-8"));
        //写入数据  并输出
        exportExcel(wb, data, response.getOutputStream());
    }


    /**
     * 写出到指定流里面
     *
     * @param wb
     * @param data
     * @param out
     * @throws Exception
     */

    public static void exportExcel(Workbook wb, ExcelData data, OutputStream out) throws IOException {
        //创建一个工作薄  格式 xlsx
        // XSSFWorkbook wb = new XSSFWorkbook();

        //Workbook wb = new HSSFWorkbook();  // 格式 xls
        try {
            //页签的名称
            String sheetName = data.getSheetName();
            if (null == sheetName) {
                sheetName = "Sheet1";
            }
            //创建一个一页 如果有就直获取 没有就创建
            Sheet sheet = wb.getSheet(sheetName);
            if (sheet == null) {
                sheet = wb.createSheet();
            wb.setSheetName(data.getSheetNum(), sheetName);
            }
            CellStyle cs = wb.createCellStyle(); // 换行的关键，自定义单元格内容换行规则
            cs.setWrapText(true);
            //写入数据
            ExcelWriteUtils.writeExcel(wb, sheet, data);

            wb.write(out);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //此处需要关闭 wb 变量
            // wb.close();
            out.close();
        }
    }


    /**
     * 只是写入数据 并没有 进行输出
     *
     * @param wb
     * @param data
     * @return
     */
    public static Workbook writerToExcel(Workbook wb, ExcelData data) {
        //页签的名称
        String sheetName = data.getSheetName();
        if (null == sheetName) {
            throw new RuntimeException("页签名称不能为空");
        }
        //创建一个一页 如果有就直获取 没有就创建
        Sheet sheet = wb.getSheet(sheetName);
        if (sheet == null) {
            sheet = wb.createSheet();
            //在这里设置名字的原因 自己创建的sheet
            wb.setSheetName(data.getSheetNum(), sheetName);
        }
        //写入数据
        ExcelWriteUtils.writeExcel(wb, sheet, data);
        return wb;
    }


    public static Workbook getWorkbook(ExcelType excelType) {
        if (excelType==null){
            return new XSSFWorkbook();
        }
        if (ExcelType.xls == excelType) {
            return new HSSFWorkbook();
        } else if (ExcelType.xlsx == excelType) {
            return new XSSFWorkbook();
        } else {
            return new XSSFWorkbook();
        }
    }

    public enum ExcelType {
        xlsx, xls
    }

}
