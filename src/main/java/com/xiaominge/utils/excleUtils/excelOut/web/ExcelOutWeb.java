package com.xiaominge.utils.excleUtils.excelOut.web;

import com.xiaominge.exception.ParameterRuntimeException;
import com.xiaominge.utils.excleUtils.excelOut.ExcelData;
import com.xiaominge.utils.excleUtils.excelOut.ExcelOutputUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.CollectionUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

/**
 * @program: case_system_parent
 * @description: 网页版
 * @author: Administrator
 * @create: 2020-07-28 16:23
 **/


public class ExcelOutWeb<T> {

    final  Field[] fields;

    public ExcelOutWeb(Class t) {
     this. fields = t.getDeclaredFields();
    }

    public void exportExcel(Workbook wb, List<T> dataList, Map<String, String> filedToDesc) throws IllegalAccessException {
        if (filedToDesc == null || filedToDesc.size() == 0) {
            throw ParameterRuntimeException.throwException("请选择需要导出的数据");
        }
        ExcelData excelData = new ExcelData();

        List<String> title = excelData.CreateTitle();
        List<String> fieldName = new ArrayList<>();
        filedToDesc.entrySet().forEach(stringStringEntry -> {
            if (StringUtils.isBlank(stringStringEntry.getKey())) {
                ParameterRuntimeException.throwException("字段名称不能为空");
            }
            fieldName.add(stringStringEntry.getKey());
            title.add(StringUtils.isBlank(stringStringEntry.getValue()) ? "" : stringStringEntry.getValue());
        });
        //获取内容
        if (!CollectionUtils.isEmpty(dataList)) {
            for (T one : dataList) {
                ConcurrentMap<String, Field> nameToFieldMap = Arrays.stream(fields).collect(Collectors.toConcurrentMap(Field::getName, field -> field));
                List<ExcelData.CellDataInfo> oneRow = excelData.createOneRow();
                for (String fileName : fieldName) {
                    Field field = nameToFieldMap.get(fileName);
                    field.setAccessible(true);
                    Class fieldType = field.getType();
                    boolean primitive = fieldType.isPrimitive();
                    if (primitive
                            || fieldType.getPackage().getName().equals("java.lang")
                            || fieldType.getPackage().getName().equals("java.util")) {
                        Object filedValue = field.get(one);
                        oneRow.add(excelData.createCellDataInfo(filedValue));
                    } else {
                        throw ParameterRuntimeException.throwException("自定义类型的对象不能直接写入excel");
                    }
                }
            }
        }
        ExcelOutputUtils.writerToExcel(wb, excelData);
    }

    /**
     * 导出excel
     *
     * @param outputStream 输出流
     * @param dataList     数据列表
     * @param filedToDesc  key 属性名字  value 描述
     * @throws Exception 异常
     */
    public void exportExcel(OutputStream outputStream, List<T> dataList, Map<String, String> filedToDesc, ExcelType excelType) throws IllegalAccessException, IOException {
        Workbook workbook = getWorkbook(excelType);
        exportExcel(workbook, dataList, filedToDesc);
        workbook.write(outputStream);
    }

    /**
     * 导出excel
     *
     * @param dataList      数据列表
     * @param excelWebBeans 界面选择的属性名 和标题的对应关系
     * @param response      响应
     * @param fileName      文件名称
     * @throws Exception 异常
     */
    public void exportExcel(List<T> dataList, List<ExcelWebBean> excelWebBeans, HttpServletResponse response, String fileName) throws IllegalAccessException, IOException {
        // 告诉浏览器用什么软件可以打开此文件
        response.setHeader("content-Type", "application/vnd.ms-excel");
        // 下载文件的默认名称
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "utf-8"));
        XSSFWorkbook wb = new XSSFWorkbook();

        Map<String, String> filedToDesc = new HashMap<>();
        if (!CollectionUtils.isEmpty(excelWebBeans)) {
            for (ExcelWebBean excelWebBean : excelWebBeans) {
                filedToDesc.put(excelWebBean.getFieldName(), excelWebBean.getFieldDesc());
            }
        }
        exportExcel(wb, dataList, filedToDesc);
        ServletOutputStream outputStream = response.getOutputStream();
        wb.write(outputStream);
        outputStream.close();
        //  wb.close();

    }
/*

    public void test() throws Exception {
        List<UserInfo> userInfos = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            if (i / 2 == 0) {
                UserInfo userInfo = new UserInfo("name" + i, i, 1);
                userInfos.add(userInfo);
            } else {
                UserInfo userInfo = new UserInfo("name" + i, i, 0);
                userInfos.add(userInfo);
            }
        }
        List<ExcelWebBean> clou = new ArrayList() {{
            add(new ExcelWebBean("userName", "用户名称"));
            add(new ExcelWebBean("idCard", "身份证号码"));
            add(new ExcelWebBean("sex", "性别"));
        }};

        ExcelOutWeb excelOutWeb = new ExcelOutWeb();
        XSSFWorkbook wb = new XSSFWorkbook();

        excelOutWeb.exportExcel(wb,
                userInfos,
                clou);
        wb.write(new FileOutputStream("D:/桌面/test.xlsx"));
    }

    public static void main(String[] args) throws Exception {
        ExcelOutWeb excelOutWeb = new ExcelOutWeb();
        excelOutWeb.test();

    }

    @Data
    @AllArgsConstructor
    public static class UserInfo {
        @ExcelTitleName("名称")
        private String userName;

        @ExcelTitleName("身份证")
        private int idCard;

        private int sex;
    }
*/

    public Workbook getWorkbook(ExcelType excelType) {
        if (excelType == null) {
            return new XSSFWorkbook();
        }
        if (ExcelType.xls == excelType) {
            return new HSSFWorkbook();
        } else if (ExcelType.xlsx == excelType) {
            return new XSSFWorkbook();
        } else {
            return new XSSFWorkbook();
        }
    }

    public enum ExcelType {
        xlsx, xls
    }

}
