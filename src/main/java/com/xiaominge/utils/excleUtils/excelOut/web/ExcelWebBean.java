package com.xiaominge.utils.excleUtils.excelOut.web;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @program: case_system_parent
 * @description:
 * @author: Administrator
 * @create: 2020-07-28 16:25
 **/


@Getter
@AllArgsConstructor
public class ExcelWebBean {
    /**
     * 属性的名称
     */
    private String fieldName;

    /**
     * 属性导出标题
     */
    private String fieldDesc;


}
