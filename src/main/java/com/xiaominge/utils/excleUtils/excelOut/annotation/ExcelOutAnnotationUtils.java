package com.xiaominge.utils.excleUtils.excelOut.annotation;

import com.xiaominge.exception.ParameterRuntimeException;
import com.xiaominge.utils.excleUtils.excelOut.ExcelData;
import com.xiaominge.utils.excleUtils.excelOut.ExcelOutputUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.CollectionUtils;

import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

/**
 * @program: case_system_parent
 * @description: 注解版本 导出excel
 * @author: Administrator
 * @create: 2020-07-28 09:30
 **/

public class ExcelOutAnnotationUtils<T> {

    //类型
    private Class clazz;
    final Field[] fields;

    public ExcelOutAnnotationUtils(Class clazz) {
        fields = clazz.getDeclaredFields();
        if (Arrays.stream(fields).filter(field -> field.getAnnotation(ExcelTitle.class) != null).count() == 0) {
            throw ParameterRuntimeException.throwException("对象中缺少注解 " + ExcelTitle.class.getName());
        }
        this.clazz = clazz;
    }


    /**
     * 导出方法
     *
     * @param wb         excel 对象
     * @param dataList   数据对象
     * @param fieldNames 需要导出的属性名称
     * @throws Exception
     */
    public void exportExcel(XSSFWorkbook wb, List<T> dataList, List<String> fieldNames) throws Exception {
        //注解版本里面的  主要就是构建exclDate对象
        ConcurrentMap<String, Field> title_nameToFieldMap = Arrays.stream(fields).collect(Collectors.toConcurrentMap(Field::getName, field -> field));

        if (fieldNames == null || fieldNames.size() == 0) {
            //如果没有指定 导出所有的
            fieldNames = new ArrayList<>(title_nameToFieldMap.keySet());
        }

        ExcelData excelData = new ExcelData();
        //标题
        List<String> titles = excelData.CreateTitle();
        //允许导出的字段
        List<String> allowFileNames = new ArrayList<>();
        for (String fileName : fieldNames) {
            Field field = title_nameToFieldMap.get(fileName);
            if (field == null) { //如果要导出的属性不存在 直接跳出
                continue;
            }
            ExcelTitle annotation = field.getAnnotation(ExcelTitle.class);
            if (annotation != null) {
                String value = annotation.value();
                titles.add(value);
                allowFileNames.add(fileName);
            }
        }

        //获取内容
        if (!CollectionUtils.isEmpty(dataList)) {
            for (T one : dataList) {
                Field[] fields = one.getClass().getDeclaredFields();
                ConcurrentMap<String, Field> nameToFieldMap = Arrays.stream(fields).collect(Collectors.toConcurrentMap(Field::getName, field -> field));
                List<ExcelData.CellDataInfo> oneRow = excelData.createOneRow();

                for (String fileName : allowFileNames) {

                    Field field = nameToFieldMap.get(fileName);
                    field.setAccessible(true);
                    Class fieldType = field.getType();
                    boolean primitive = fieldType.isPrimitive();

                    if (primitive || fieldType.getPackage().getName().equals("java.lang") || fieldType.getPackage().getName().equals("java.util")) {
                        Object filedValue = field.get(one);

                        oneRow.add(excelData.createCellDataInfo(filedValue));
                    } else {
                        throw ParameterRuntimeException.throwException("自定义类型的对象不能直接写入excel");
                    }
                }
            }
        }
        ExcelOutputUtils.writerToExcel(wb, excelData);
    }

    public void exportExcel(List<T> dataList, List<String> fieldNames, OutputStream outputStream) throws Exception {
        XSSFWorkbook wb = new XSSFWorkbook();
        exportExcel(wb, dataList, fieldNames);
        wb.write(outputStream);
    }




    /*
    public void test() throws Exception {
        List<UserInfo> userInfos = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            if (i / 2 == 0) {
                UserInfo userInfo = new UserInfo("name" + i, i, 1);
                userInfos.add(userInfo);
            } else {
                UserInfo userInfo = new UserInfo("name" + i, i, 0);
                userInfos.add(userInfo);
            }
        }
        List<String> clou = new ArrayList() {{
            add("userName");
            add("sex");
            add("idCard");
        }};

        ExcelOutAnnoUtils excelOutAnnoUtils = new ExcelOutAnnoUtils(UserInfo.class);
        XSSFWorkbook wb = new XSSFWorkbook();

        excelOutAnnoUtils.exportExcel(wb,
                userInfos,
                clou);
        wb.write(new FileOutputStream("D:/桌面/test.xlsx"));
    }

    public static void main(String[] args) throws Exception {
        ExcelOutAnnoUtils excelOutAnnoUtils = new ExcelOutAnnoUtils(UserInfo.class);

        excelOutAnnoUtils.test();

    }

    @Data
    @AllArgsConstructor
    public static class UserInfo {
        @ExcelTitleName("名称")
        private String userName;

        @ExcelTitleName("身份证")
        private int idCard;

        private int sex;
    }*/
}