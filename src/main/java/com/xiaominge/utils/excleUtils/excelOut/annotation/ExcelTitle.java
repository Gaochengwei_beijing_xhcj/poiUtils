package com.xiaominge.utils.excleUtils.excelOut.annotation;

import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelTitle {
    String value();
}
