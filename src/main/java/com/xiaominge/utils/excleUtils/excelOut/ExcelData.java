package com.xiaominge.utils.excleUtils.excelOut;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author xiaomin
 * @Description
 * @Date 2019/5/9 9:18
 */
@Getter
@Setter
public class ExcelData implements Serializable {
    private static final long serialVersionUID = 4444017239100620999L;

    // 表头
    private List<String> titles;

    //页签的数

    private int SheetNum = 0;

    // 页签名称
    private String SheetName = "sheet1";

    //数据开始写的行 不是标题
    private Integer rowIndex;

    // 数据
    private List<List<CellDataInfo>> rows;


    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public List<String> CreateTitle() {
        this.titles = new ArrayList<>();
        return titles;
    }


    public List<CellDataInfo> createOneRow() {
        if (this.rows == null) {
            this.rows = new ArrayList<>();
        }
        List<CellDataInfo> oneRow = new ArrayList<>();
        rows.add(oneRow);
        return oneRow;
    }


    public CellDataInfo createCellDataInfo(Object cellData) {
        return new CellDataInfo(cellData);
    }


    @Getter
    @NoArgsConstructor
    public class CellDataInfo {

        private Object data;


        public void setData(Object data) {
            this.data = data;
        }


        public CellDataInfo(Object data) {
            this.data = data;
        }
    }


}
