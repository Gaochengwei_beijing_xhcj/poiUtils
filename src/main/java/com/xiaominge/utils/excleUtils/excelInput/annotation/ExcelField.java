package com.xiaominge.utils.excleUtils.excelInput.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)

public @interface ExcelField {

    /**
     * excel 的标题
     *
     * @return
     */
    String title();

    /**
     * 字段在对应excel 列的序号 从1开始
     */
    int index();

    String datePattern() default "yyyy-MM-dd HH:mm:ss";

    enum DataType {
        String, Number, Date,no,
    }

    /**
     * 数据类型，可以是String，Number(数字型)，Date等类型
     *
     * @return
     */
    DataType type() default DataType.no;

}
