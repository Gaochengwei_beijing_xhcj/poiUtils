package com.xiaominge.utils.excleUtils.excelMerge;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @program: parent
 * @description: 一行数据信息
 * @author: xiaominge
 * @create: 2019-09-10 10:52
 **/

@Getter
@Setter
@AllArgsConstructor
public class ExcelRowInfo {

    /**
     * 行号 EXCEL 中 行号从0 开始
     */
    Integer rowIndex;
    /**
     * 一行数据 元素是一个个单元格
     */
    List<ExcelCellInfo> excelCellInfos;


}
