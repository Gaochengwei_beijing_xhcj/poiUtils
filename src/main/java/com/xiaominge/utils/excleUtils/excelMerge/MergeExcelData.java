package com.xiaominge.utils.excleUtils.excelMerge;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: parent
 * @description: excel 数据对象
 * @author: xiaominge
 * @create: 2019-09-10 09:49
 **/
@Getter
@Setter
public class MergeExcelData {

    /**
     * 所有 标题  里面是一行标题
     * 根据标题里面是否有合并单元格信息 进行合并单元格.
     */
    ExcelRowInfo title;
    /**
     * 所有行数据  里面是一行 数据
     */
    List<ExcelRowInfo> rows;
    /**
     * 页签的数
     */
    private Integer SheetNum = 0;
    /**
     * 页签名称
     */
    private String sheetName;

    /**
     * 所有需要合并单元格的信息  在数据渲染完成之后进行合并单元格
     */
    List<ExcelMergeData> allExcelMergeData = new ArrayList<>();

    /**
     * 添加一行数据
     *
     * @param excelRowInfo
     */
    public void addRowDate(ExcelRowInfo excelRowInfo) {
        if (rows == null) {
            this.rows = new ArrayList<>();
        }
        this.rows.add(excelRowInfo);
    }

    /**
     * 创建一行数据
     * @param rowIndex
     * @param excelCellInfos
     * @return
     */
    public ExcelRowInfo createOneRow(int rowIndex, List<ExcelCellInfo> excelCellInfos) {
        ExcelRowInfo excelRowInfo = new ExcelRowInfo(rowIndex, excelCellInfos);
        return  excelRowInfo;
    }
}
