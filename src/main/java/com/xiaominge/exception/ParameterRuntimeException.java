package com.xiaominge.exception;

/**
 * @program: poi-utils
 * @description:
 * @author: xiaominge
 * @create: 2022-01-13 16:13
 **/

public class ParameterRuntimeException extends RuntimeException {

    public ParameterRuntimeException(String msg) {
        super(msg);
    }

    public static ParameterRuntimeException throwException(String msg) {

        throw new ParameterRuntimeException(msg);
    }

}
